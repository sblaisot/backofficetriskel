# Backoffice Ciné

Pilotage de cinéma associatif, organisation des séances et affectation des bénévoles aux séances

## Installation

```bash
python3 -m venv .venv
. .venv/bin/activate
pip install wheel
pip install -r requirements.txt
```

## Base de données

Initialisez la base de données et peuplée la avec les données de test :

```bash
flask db upgrade
flask testdata populate
```

## Lancement

```bash
flask run
```

Vous pouvez ensuite accéder au site à l'adresse <http://localhost:5000>

Cette configuration ne convient pas pour une exécution en production.
Il faut utiliser un serveur wSGI pour cela (documentation à venir)

## Configuration de Develoment

Pour installer tous les modules nécessaires au développement :

```bash
. .venv/bin/activate
pip install -r requirements-dev.txt
```

Vous pouvez ensuite le lancer en mode debug (qui apporte l'auto-rechergement en cas de changement de sources
et l'affichage des traces d'erreurs dans el navigateur)

```bash
flask run --debug
```
