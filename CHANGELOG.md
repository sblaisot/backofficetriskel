# Release History

## [0.0.8] - Unreleased

### Added



## [0.0.7] - 2024-11-30

### Added

- Theater management:
  - Add Theater message of the day (MOTD)
  - Add user invitation
  - Add show types
  - Add show comments with selectable audience
  - Add data model for film comments with selectable audience (no interface yet
    to edit comment)
- Improved application layout and main menu
- Show theater message of the day on index page
- Improved show list:
  - one column for assignable positions and an other column for all the other positions
  - Add link to contact details for assigned user in show list
  - Film rating
  - Show type
  - Film and show comments

## [0.0.6] - 2024-11-04

### Added

- Add assignations to shows
- Add user status to user edit form (theater admin)
- Add data model documentation
- Add name, phone and profile picture fields to users
- Add a cli command to inject test data
- Add mugshots page
- Make profile data self-editable for all users
- Add "My shows" list showing all show on which I am assigned to a position
- Allow to add / edit / delete show in theater admin
- Add data import tab in theater admin page

## [0.0.5] - 2023-12-29

### Added

- LOCAL_VERSION country in film list
- Theater management
  - Handle groups and permissions management
  - Assign user to group

## [0.0.4] - 2023-12-26

### Added

- Theater management
  - Added user edition and deletion
  - Added user status management (active, disabled, banned)
  - Added positions management (List, Add, Remove, Update)
  - Added shows list

### Fixed

- Properly handle auditorium integrity constraints (unique auditorium name and
  CDS number for a given theater)

## [0.0.3] - 2023-12-24

### Added

- Film catalog with search function
- Theater management page with tabs
  - Theater information
  - User list
  - Auditorium list (create, read, update, delete)
- Admin panel with theaters management

### Fixed

- Improve film detail page responsiveness
- Data loader improvements
  - Properly handle non-sqlite databases
  - Add current live data in the example file
- Fix foreign key constraints in DB and cascaded deletes

### Chore

- Renamed jinja templates to ease syntax highlighting

## [0.0.2] - 2023-12-21

### Added

- Added Group, Film, Show and Position to models
- Added temporary data loader from example file and live CDS data
- Improved profile page with email address and password update
- Added film details page

### Chore

- Properly handle app name and version

## [0.0.1] - 2023-12-20

### Added

- Initial release, just a skeleton
