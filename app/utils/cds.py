import sqlalchemy as sa
from dateutil.parser import parse
from app import db
from app.models import Auditorium, Film, Show

def insert_shows_from_cds(theater, shows, verbose=False):
    import_log = []

    for show in shows:
        # Create Film if not found
        film_title = show.get("FilmTitle")
        film_CDS_id = show.get("FilmId")
        if film_CDS_id:
            film_CDS_id = int(film_CDS_id)
        film_country = show.get("FilmCountry")
        if film_country == "null":
            film_country = None
        f = db.session.scalar(
            sa.select(Film).where(Film.cds_id == film_CDS_id)
        )
        if f is None:
            # Film not found by id, now search by title
            f = db.session.scalar(
                sa.select(Film).where(Film.title == film_title)
            )
        if f is None:
            # Film not found, create it
            if verbose:
                print(f"  Nouveau film ajouté : {film_title}")
            else:
                import_log.append(f"Nouveau film ajouté : {film_title}")
            genre = show.get("FilmGenre")
            if genre == "null":
                genre = None
            f = Film(
                title=film_title,
                storyline=show.get("FilmStoryline"),
                rating=show.get("FilmRating"),
                visa=show.get("FilmVisa"),
                cds_id=film_CDS_id,
                allocine_id=show.get("FilmAllocineId"),
                trailerUrl=show.get("FilmTrailer"),
                posterUrl=show.get("FilmPoster"),
                country=film_country,
                genre=genre,
                director=show.get("FilmDirector"),
                cast=show.get("FilmCast"),
                duration=show.get("FilmDuration"),
            )
            db.session.add(f)

        # Look for Auditorium by cds_id
        auditorium_cds_id = int(show.get("AuditoriumNumber"))
        auditorium = db.session.scalar(
            sa.select(Auditorium).where(
                (Auditorium.theater == theater) & (Auditorium.cds_auditorium_number == auditorium_cds_id)
            )
        )
        if auditorium is None:
            if verbose:
                print(f"⚠️ Impossible de trouver la salle avec l'ID CDS {auditorium_cds_id}, séance ignorée")
            else:
                import_log.append(f"⚠️ Impossible de trouver la salle avec l'ID CDS {auditorium_cds_id}, séance ignorée")
            continue

        # Create Show if not found
        show_cds_id = show.get("ShowId")
        if show_cds_id:
            show_cds_id = int(show_cds_id)
        s = db.session.scalar(
            sa.select(Show).where(
                (Show.theater == theater) & (Show.cds_id == show_cds_id)
            )
        )
        if s is None:
            show_start = parse(show.get("ShowStart"))
            if verbose:
                print(f"  Nouvelle séance ajoutée : {f.title} le {show_start.strftime('%d/%m/%Y à %H:%M')} en {auditorium.name}")
            else:
                import_log.append(f"Nouvelle séance ajoutée : {f.title} le {show_start.strftime('%d/%m/%Y à %H:%M')} en {auditorium.name}")
            s = Show(
                start=show_start,
                end=parse(show.get("ShowEnd")),
                exclude_from_internet=(
                    show.get("ExcludeFromInternet") != "false"
                ),
                cds_id=show_cds_id,
                theater=theater,
                auditorium=auditorium,
                film=f,
                film_depth=show.get("FilmDepth"),
                film_audio=show.get("FilmAudio"),
                film_version=show.get("FilmVersion"),
            )
            db.session.add(s)
        db.session.commit()
    if verbose:
        return
    else:
        return import_log
