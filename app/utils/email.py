from threading import Thread
from flask import current_app, render_template
from flask_mail import Message
from app import mail


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    Thread(
        target=send_async_email, args=(current_app._get_current_object(), msg)
    ).start()


def send_invitation_email(user, token):
    send_email(
        '[Cinéma] Invitation à rejoindre la plateforme',
        sender=current_app.config["MAIL_FROM"],
        recipients=[user.email],
        text_body=render_template('email/invite_user.txt', user=user, token=token),
        html_body=render_template('email/invite_user.html.j2', user=user, token=token)
    )
