from flask import Blueprint

bp = Blueprint(
    "admin", __name__, template_folder="templates", static_folder="static"
)

from .wrapper import admin_required
from . import routes
