import sqlalchemy as sa

from flask import request, render_template, redirect, url_for
from flask_login import login_required

from . import bp, admin_required
from .forms import TheaterSearchForm, AddTheaterForm
from app import db
from app.models import Theater, Group


@bp.route("/")
@login_required
@admin_required
def home():
    return render_template("admin/index.html.j2", title="Administration")


@bp.route("/film", methods=["GET", "POST"])
@login_required
@admin_required
def theaters():
    query = request.args.get("q", None, type=str)
    page = request.args.get("page", 1, type=int)
    sqlquery = sa.select(Theater)
    if query:
        sqlquery = sqlquery.filter(
            sa.or_(
                Theater.name.like(f"%{query}%"),
                Theater.city.like(f"%{query}%"),
            )
        )
    sqlquery = sqlquery.order_by(Theater.id.desc())
    theaters = db.paginate(sqlquery, page=page, per_page=25, error_out=False)
    next_url = (
        url_for(
            "admin.theaters", page=theaters.next_num, q=query if query else ""
        )
        if theaters.has_next
        else None
    )
    prev_url = (
        url_for(
            "admin.theaters", page=theaters.prev_num, q=query if query else ""
        )
        if theaters.has_prev
        else None
    )
    search_form = TheaterSearchForm()
    return render_template(
        "admin/theaters.html.j2",
        title="Theaters",
        theaters=theaters.items,
        pagenum=theaters.page,
        next_url=next_url,
        prev_url=prev_url,
        search_form=search_form,
    )


@bp.route("/theater/add", methods=["GET", "POST"])
@login_required
@admin_required
def add_theater():
    form = AddTheaterForm()
    if form.cancel.data:
        return redirect(url_for("admin.theaters"))
    if form.validate_on_submit():
        t = Theater(
            name=form.name.data,
            city=form.city.data,
        )
        db.session.add(t)
        g = Group(name="Administrateur", theater=t, theater_admin=True)
        db.session.add(g)
        db.session.commit()
        return redirect(url_for("admin.theaters"))
    return render_template(
        "simple_form.html.j2",
        header="Ajout de cinéma",
        title="Ajout de cinéma",
        form=form,
    )


@bp.route("/theater/<int:theater_id>")
@login_required
@admin_required
def remove_theater(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    db.session.delete(t)
    db.session.commit()
    return redirect(url_for("admin.theaters", id=t.id))


@bp.route(
    "/theater/<int:theater_id>/edit",
    methods=["GET", "POST"],
)
@login_required
@admin_required
def edit_theater(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = AddTheaterForm()
    if form.cancel.data:
        return redirect(url_for("admin.theaters", id=t.id))
    if form.validate_on_submit():
        t.name = form.name.data
        t.city = form.city.data
        db.session.commit()
        return redirect(url_for("admin.theaters", id=t.id))
    form.name.data = t.name
    form.city.data = t.city
    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header=f"Edition Cinéma {t.name}",
        form=form,
    )
