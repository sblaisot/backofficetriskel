from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class TheaterSearchForm(FlaskForm):
    q = StringField("Recherche")

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "meta" not in kwargs:
            kwargs["meta"] = {"csrf": False}
        super(TheaterSearchForm, self).__init__(*args, **kwargs)


class AddTheaterForm(FlaskForm):
    name = StringField("Nom du cinéma", validators=[DataRequired()])
    city = StringField("Ville", validators=[DataRequired()])
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )
    submit = SubmitField("Valider")
