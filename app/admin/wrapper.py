from flask import abort
from flask_login import current_user
from functools import wraps


def admin_required(func):
    """
    If you decorate a view with this, it will ensure that the current user has
    admin privileges. For example:
        @app.route('/post')
        @login_required
        @admin_required
        def post():
            pass
    """

    @wraps(func)
    def wrapped(*args, **kwargs):
        if not current_user.siteAdmin:
            abort(403, "Vous n'êtes pas autorisé à accéder à cette URL.")
        return func(*args, **kwargs)

    return wrapped
