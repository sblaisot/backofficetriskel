
import sqlalchemy as sa
import sys

from app import db
from app.models import Theater

def commands(app):
    @app.cli.group('theater')
    def theater_group():
        """Theater management commands."""
        pass

    @theater_group.command('import_from_cds')
    def import_from_cds_command():
        """Import shows from CDS for a given theater."""
        theaters = db.session.scalars(sa.select(Theater)).all()
        if not theaters:
            print("No theater found in DB")
            sys.exit(1)
        for theater in theaters:
            print(f"Loading data from CDS for theater {theater.name} in {theater.city}")
            theater.import_from_cds(verbose=True)
