import sqlalchemy as sa
import xmltodict
from faker import Faker
from flask import current_app
from flask.cli import with_appcontext

from app import db
from app.models import (
    Theater,
    Auditorium,
    Position,
    PermissionTypes,
    Group,
    Permission,
    User,
    UserStatus,
    Assignable,
)
from app.utils.cds import insert_shows_from_cds


def commands(app):
    @app.cli.group('testdata')
    def testdata_group():
        """Test data management commands."""
        pass

    def create_theater():
        print("Creating theater")
        existing_theater = db.session.scalar(
            sa.select(Theater).where(
                Theater.name == "Le Triskel",
                Theater.city == "Betton"
            )
        )
        if existing_theater is None:
            theater = Theater(
                name="Le Triskel",
                city="Betton",
                cds_import_url="https://bettonletriskel.cineoffice.fr/TMSexport/bettonletriskel"
            )
            db.session.add(theater)
            db.session.commit()
            print(f"  Created theater: {theater}")
        else:
            print(f"  Using existing theater: {existing_theater}")
            theater = existing_theater
        return theater

    def create_auditorium(theater):
        print("Creating auditorium")
        existing_auditorium = db.session.scalar(
            sa.select(Auditorium).where(
                Auditorium.theater == theater,
                Auditorium.name == "Salle 1"
            )
        )
        if existing_auditorium is None:
            auditorium = Auditorium(
                name="Salle 1",
                capacity=193,
                theater=theater,
                cds_auditorium_number=1
            )
            db.session.add(auditorium)
            db.session.commit()
            print(f"  Created auditorium: {auditorium}")
        else:
            print(f"  Using existing auditorium: {existing_auditorium}")
            auditorium = existing_auditorium
        return auditorium

    def create_positions(theater):
        print("Creating positions")
        positions = {}
        for p in [
            "Responsable de caisse",
            "Caissier",
            "Contrôle",
            "Bonbons",
            "Projectionniste"
        ]:
            existing_position = db.session.scalar(
                sa.select(Position).where(Position.theater == theater, Position.name == p)
            )
            if existing_position is None:
                positions[p] = Position(name=p, theater=theater)
                db.session.add(positions[p])
                print(f"  Created position: {positions[p]}")
            else:
                print(f"  Using existing position: {existing_position}")
                positions[p] = existing_position
        db.session.commit()
        return positions

    def create_groups(theater, positions):
        print("Creating groups")
        groups = {}
        for g in [
            {
                "name": "Administrateur",
                "theater_admin": True,
                "perms": {},
                "assignables": [],
            },
            {
                "name": "Responsables de caisse",
                "theater_admin": False,
                "perms": {
                    "Responsable de caisse": PermissionTypes.TAKE,
                    "Caissier": PermissionTypes.ASSIGN,
                    "Contrôle": PermissionTypes.ASSIGN,
                    "Bonbons": PermissionTypes.ASSIGN,
                    "Projectionniste": PermissionTypes.READ,
                },
                "assignables": [
                    positions["Responsable de caisse"],
                    positions["Caissier"],
                    positions["Contrôle"],
                    positions["Bonbons"],
                ],
            },
            {
                "name": "Bénévoles accueil",
                "theater_admin": False,
                "perms": {
                    "Responsable de caisse": PermissionTypes.READ,
                    "Caissier": PermissionTypes.READ,
                    "Contrôle": PermissionTypes.TAKE,
                    "Bonbons": PermissionTypes.TAKE,
                    "Projectionniste": PermissionTypes.READ,
                },
                "assignables": [
                    positions["Contrôle"],
                    positions["Bonbons"],
                ],
            },
            {
                "name": "Caissiers",
                "theater_admin": False,
                "perms": {
                    "Responsable de caisse": PermissionTypes.READ,
                    "Caissier": PermissionTypes.TAKE,
                    "Contrôle": PermissionTypes.TAKE,
                    "Bonbons": PermissionTypes.TAKE,
                    "Projectionniste": PermissionTypes.READ,
                },
                "assignables": [
                    positions["Caissier"],
                    positions["Contrôle"],
                    positions["Bonbons"],
                ],
            },
            {
                "name": "Projectionnistes",
                "theater_admin": False,
                "perms": {
                    "Responsable de caisse": PermissionTypes.READ,
                    "Caissier": PermissionTypes.READ,
                    "Contrôle": PermissionTypes.READ,
                    "Bonbons": PermissionTypes.READ,
                    "Projectionniste": PermissionTypes.TAKE,
                },
                "assignables": [
                    positions["Projectionniste"],
                ],
            },
            {
                "name": "Responsable Projectionnistes",
                "theater_admin": False,
                "perms": {
                    "Responsable de caisse": PermissionTypes.READ,
                    "Caissier": PermissionTypes.READ,
                    "Contrôle": PermissionTypes.READ,
                    "Bonbons": PermissionTypes.READ,
                    "Projectionniste": PermissionTypes.ASSIGN,
                },
                "assignables": [
                    positions["Projectionniste"],
                ],
            },
            {
                "name": "Responsables des bénévoles d'accueil",
                "theater_admin": False,
                "perms": {
                    "Responsable de caisse": PermissionTypes.ASSIGN,
                    "Caissier": PermissionTypes.ASSIGN,
                    "Contrôle": PermissionTypes.ASSIGN,
                    "Bonbons": PermissionTypes.ASSIGN,
                    "Projectionniste": PermissionTypes.READ,
                },
                "assignables": [
                    positions["Responsable de caisse"],
                    positions["Caissier"],
                    positions["Contrôle"],
                    positions["Bonbons"],
                ],
            },
        ]:
            existing_group = db.session.scalar(
                sa.select(Group).where(Group.theater == theater, Group.name == g["name"])
            )
            if existing_group is None:
                group = Group(name=g["name"], theater=theater, theater_admin=g["theater_admin"])
                db.session.add(group)
                groups[g["name"]] = group
                print(f"  Created group: {group}")
                for pos, perm in g["perms"].items():
                    if perm == PermissionTypes.NONE:
                        continue
                    db.session.add(
                        Permission(theater=theater, group=group, position=positions[pos], permission=perm)
                    )
                for pos in g["assignables"]:
                    db.session.add(Assignable(group=group, position=pos))
            else:
                print(f"  Using existing group: {existing_group}")
                groups[g["name"]] = existing_group
        db.session.commit()
        return groups

    def create_users(theater, groups):
        print("Creating users")
        # Users
        users = {}
        for u in [
            {
                "username": "sblaisot",
                "first_name": "Sebastien",
                "last_name": "Blaisot",
                "email": "sebastien@blaisot.org",
                "mobile_phone": "0671286574",
                "telephone": "0951795795",
                "siteAdmin": True,
                "group": groups["Administrateur"],
                "password": "foobar",
            },
            {
                "username": "admin",
                "siteAdmin": False,
                "group": groups["Administrateur"],
            },
            {
                "username": "resp_caisse",
                "siteAdmin": False,
                "group": groups["Responsables de caisse"],
            },
            {
                "username": "accueil",
                "siteAdmin": False,
                "group": groups["Bénévoles accueil"],
            },
            {
                "username": "caissier",
                "siteAdmin": False,
                "group": groups["Caissiers"],
            },
            {
                "username": "projectionniste",
                "siteAdmin": False,
                "group": groups["Projectionnistes"],
            },
            {
                "username": "resp_proj",
                "siteAdmin": False,
                "group": groups["Responsable Projectionnistes"],
            },
            {
                "username": "resp_accueil",
                "siteAdmin": False,
                "group": groups["Responsables des bénévoles d'accueil"],
            },
        ]:
            existing_user = db.session.scalar(
                sa.select(User).where(
                    (User.theater == theater) & (User.username == u["username"])

                )
            )
            if existing_user is None:
                fake = Faker(locale="fr_FR")
                first_name = u.get("first_name", u["username"].capitalize())
                last_name = u.get("last_name", "Triskel")
                email = u.get("email", fake.email())
                mobile_phone = u.get("mobile_phone", fake.bothify(text="06########"))
                telephone = u.get("telephone", fake.bothify(text="02########"))
                user = User(
                    username=u["username"],
                    email=email,
                    first_name=first_name,
                    last_name=last_name,
                    mobile_phone=mobile_phone,
                    telephone=telephone,
                    siteAdmin=u["siteAdmin"],
                    status=UserStatus.ACTIVE,
                    theater=theater,
                    group=u["group"],
                )
                user.set_password(u.get("password", u["username"]))
                db.session.add(user)
                print(f"  Created user: {user}")
                users[u["username"]] = user
            else:
                print(f"  Using existing user: {existing_user}")
                users[u["username"]] = existing_user
        db.session.commit()
        return users

    def import_show_data(theater):
        print("Loading show data from example file")
        with open("Exemple-Export-Program-V2.xml") as fd:
            shows = xmltodict.parse(fd.read())["Shows"]["Show"]
            insert_shows_from_cds(theater, shows, verbose=True)

    @testdata_group.command('populate')
    def populate_command():
        """Populate the database with initial data."""
        theater = create_theater()
        auditorium = create_auditorium(theater)
        positions = create_positions(theater)
        groups = create_groups(theater, positions)
        users = create_users(theater, groups)
        import_show_data(theater)
        # Refresh shows directly from CDS
        print("Refreshing shows from CDS")
        theater.import_from_cds(verbose=True)
