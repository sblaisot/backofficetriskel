from .testdata import commands as testdata_commands
from .theater import commands as theater_commands

def register_commands(app):
    testdata_commands(app)
    theater_commands(app)
