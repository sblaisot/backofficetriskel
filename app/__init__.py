import locale
import sqlalchemy as sa

from flask import Flask
from flask_caching import Cache
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlite3 import Connection as SQLite3Connection

from app.version import __appname__, __appversion__
from config import Config

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}


@sa.event.listens_for(sa.engine.Engine, "connect")
def _set_sqlite_pragma(dbapi_connection, connection_record):
    if isinstance(dbapi_connection, SQLite3Connection):
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()


locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
metadata = sa.MetaData(naming_convention=convention)
db = SQLAlchemy(metadata=metadata)
migrate = Migrate()
login = LoginManager()
login.login_view = "auth.login"
login.login_message = "Connectez-vous pour accéder à cette page."
mail = Mail()
cache = Cache()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.name = __appname__
    app.version = __appversion__
    app.version_string = "{} v{}".format(app.name.capitalize(), app.version)
    app.config.from_object(Config)
    app.jinja_env.add_extension("jinja2.ext.do")

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    mail.init_app(app)
    cache.init_app(app)

    # Inject app info variables to templates
    @app.context_processor
    def inject_appinfo():
        return dict(
            app=dict(
                name=app.name.capitalize(),
                version_string=app.version_string,
                version=app.version,
            )
        )

    from app.main import bp as main_bp

    app.register_blueprint(main_bp, url_prefix="/")

    from app.errors import bp as errors_bp

    app.register_blueprint(errors_bp, url_prefix="/error")

    from app.auth import bp as auth_bp

    app.register_blueprint(auth_bp, url_prefix="/auth")

    from app.admin import bp as admin_bp

    app.register_blueprint(admin_bp, url_prefix="/admin")

    return app


# from app import models
