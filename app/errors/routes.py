import os
from flask import abort
from . import bp


@bp.route("/<code>")
def error(code):
    # This route exists only when testing
    if "TESTING" not in os.environ:
        abort(404)
    abort(int(code))
