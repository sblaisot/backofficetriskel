from flask import render_template
from . import bp


@bp.app_errorhandler(400)
def bad_request_error(error):
    return (
        render_template(
            "errors/error.html.j2",
            title="Erreur 400",
            error_title="Bad Request Error",
            generic_msg="The server cannot or will not process the request due to an apparent client error.",
            message=error.description,
        ),
        400,
    )


@bp.app_errorhandler(401)
def unauthorized_error(error):
    return (
        render_template(
            "errors/error.html.j2",
            title="Erreur 401",
            error_title="Unauthorized Error",
            generic_msg="You are not authorized to access the requested URL.",
            message=error.description,
        ),
        401,
    )


@bp.app_errorhandler(403)
def forbidden_error(error):
    return (
        render_template(
            "errors/error.html.j2",
            title="Erreur 403",
            error_title="Forbidden Error",
            generic_msg="You are not authorized to access the requested URL.",
            message=error.description,
        ),
        403,
    )


@bp.app_errorhandler(404)
def not_found_error(error):
    return (
        render_template(
            "errors/error.html.j2",
            title="Erreur 404",
            error_title="Not Found Error",
            generic_msg="File Not Found.",
            message=error.description,
        ),
        404,
    )


@bp.app_errorhandler(429)
def too_many_requests_error(error):
    return (
        render_template(
            "errors/error.html.j2",
            title="Erreur 429",
            error_title="Too Many Requests",
            generic_msg="Too Many Requests.",
            message=error.description,
        ),
        429,
    )


@bp.app_errorhandler(500)
def internal_error(error):
    return (
        render_template(
            "errors/error.html.j2",
            title="Erreur 500",
            error_title="Internal error",
            generic_msg="An unexpected error has occurred.",
            message=error.description,
        ),
        500,
    )
