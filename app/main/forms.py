import sqlalchemy as sa

from flask import request
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    IntegerField,
    SubmitField,
    BooleanField,
    SelectField,
    DateTimeLocalField,
    RadioField,
    TextAreaField,
    DateTimeField,
)
from wtforms.validators import ValidationError, DataRequired, Optional, Email, Length, Regexp, URL
from flask_wtf.file import FileField, FileAllowed
import base64
from PIL import Image
from io import BytesIO
from wtforms.widgets import DateTimeLocalInput
from wtforms.fields import SelectMultipleField
from datetime import datetime, timezone

from app import db
from app.models import Auditorium, Position, User, UserStatus, Group, Film, ShowType, MOTD
from app.main.filters import pretty_user_status


class SearchForm(FlaskForm):
    q = StringField("Recherche")

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "meta" not in kwargs:
            kwargs["meta"] = {"csrf": False}
        super(SearchForm, self).__init__(*args, **kwargs)


class EditGroupForm(FlaskForm):
    name = StringField("Nom du groupe", validators=[DataRequired()])
    theater_admin = BooleanField("Administrateur du cinéma")
    submit = SubmitField("Valider")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, theater, group=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.theater = theater
        self.group = group
        if group:
            if self.name.data is None:
                self.name.data = group.name
            if self.theater_admin.raw_data is None:
                self.theater_admin.data = group.theater_admin

    def validate_name(self, name):
        if self.group is None or name.data != self.group.name:
            group = db.session.scalar(
                sa.select(Group).where(
                    (Group.theater == self.theater) & (Group.name == name.data)
                )
            )
            if group is not None:
                raise ValidationError("Un groupe de même nom existe déjà")

class EditShowtypeForm(FlaskForm):
    name = StringField("Nom du type de séance", validators=[DataRequired()])
    color = RadioField("Couleur", choices=[
        ('primary', '<span class="badge rounded-pill text-bg-primary">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'),
        ('secondary', '<span class="badge rounded-pill text-bg-secondary">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'),
        ('success', '<span class="badge rounded-pill text-bg-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'),
        ('danger', '<span class="badge rounded-pill text-bg-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'),
        ('warning', '<span class="badge rounded-pill text-bg-warning">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'),
        ('info', '<span class="badge rounded-pill text-bg-info">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'),
        ('light', '<span class="badge rounded-pill text-bg-light">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'),
        ('dark', '<span class="badge rounded-pill text-bg-dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>')
    ], render_kw={'label_safe_html': True})
    description = StringField("Description", validators=[Optional()])
    submit = SubmitField("Valider")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, theater, showtype=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.theater = theater
        self.showtype = showtype
        if showtype:
            if self.name.data is None:
                self.name.data = showtype.name
            if self.color.data is None:
                self.color.data = showtype.color
            if self.description.data is None:
                self.description.data = showtype.description

    def validate_name(self, name):
        if self.showtype is None or name.data != self.showtype.name:
            showtype = db.session.scalar(
                sa.select(ShowType).where(
                    (ShowType.theater == self.theater)
                    & (ShowType.name == name.data)
                )
            )
            if showtype is not None:
                raise ValidationError("Un type de séance de même nom existe déjà")


class EditAuditoriumForm(FlaskForm):
    name = StringField("Nom de la salle", validators=[DataRequired()])
    capacity = IntegerField("Capacité", validators=[DataRequired()])
    cds_auditorium_number = IntegerField(
        "Numéro de salle CDS", validators=[Optional()]
    )
    submit = SubmitField("Valider")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, theater, auditorium=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.theater = theater
        self.auditorium = auditorium
        if auditorium:
            if self.name.data is None:
                self.name.data = auditorium.name
            if self.capacity.data is None:
                self.capacity.data = auditorium.capacity
            if self.cds_auditorium_number.data is None:
                self.cds_auditorium_number.data = (
                    auditorium.cds_auditorium_number
                )

    def validate_name(self, name):
        if self.auditorium is None or name.data != self.auditorium.name:
            auditorium = db.session.scalar(
                sa.select(Auditorium).where(
                    (Auditorium.theater == self.theater)
                    & (Auditorium.name == name.data)
                )
            )
            if auditorium is not None:
                raise ValidationError("Une salle de même nom existe déjà")

    def validate_cds_auditorium_number(self, cds_auditorium_number):
        if (
            self.auditorium is None
            or cds_auditorium_number.data
            != self.auditorium.cds_auditorium_number
        ):
            auditorium = db.session.scalar(
                sa.select(Auditorium).where(
                    (Auditorium.theater == self.theater)
                    & (
                        Auditorium.cds_auditorium_number
                        == cds_auditorium_number.data
                    )
                )
            )
            if auditorium is not None:
                raise ValidationError(
                    "Une salle de même identifiant CDS existe déjà"
                )


class EditUserForm(FlaskForm):
    username = StringField("Pseudo", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])
    first_name = StringField("Prénom", validators=[Optional()])
    last_name = StringField("Nom", validators=[Optional()])
    telephone = StringField('Téléphone fixe', validators=[Optional(), Length(min=10, max=14), Regexp(regex='^[0-9 ]+$')])
    mobile_phone = StringField("Téléphone mobile", validators=[Optional(), Length(min=10, max=14), Regexp(regex='^[0-9 ]+$')])
    profile_picture_file = FileField("Photo de profil", validators=[
        Optional(),
        FileAllowed(['jpg', 'jpeg', 'png'], 'Images uniquement!')
    ])
    group = SelectField("Groupe", coerce=int)
    status = SelectField("Statut", coerce=str, choices=[(s.name, pretty_user_status(s)) for s in UserStatus], render_kw={'disabled':''})
    submit = SubmitField("Valider")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, original_user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.original_user = original_user
        if original_user:
            if self.username.data is None:
                self.username.data = original_user.username
            if self.email.data is None:
                self.email.data = original_user.email
            if self.first_name.data is None:
                self.first_name.data = original_user.first_name
            if self.last_name.data is None:
                self.last_name.data = original_user.last_name
            if self.telephone.data is None:
                self.telephone.data = original_user.telephone
            if self.mobile_phone.data is None:
                self.mobile_phone.data = original_user.mobile_phone
            if self.group.data is None:
                if original_user.group is None:
                    self.group.data = 0
                else:
                    self.group.data = original_user.group_id
            if self.status.data is None:
                self.status.data = original_user.status.name

    def process_profile_picture(self):
        """Process the uploaded profile picture and return base64 encoded string"""
        if self.profile_picture_file.data:
            # Read the uploaded file
            image = Image.open(self.profile_picture_file.data)

            # Resize image to a reasonable size (e.g., 200x200)
            image.thumbnail((200, 200))

            # Save the image to a bytes buffer
            buffer = BytesIO()
            image.save(buffer, format="PNG")

            # Encode as base64
            img_str = base64.b64encode(buffer.getvalue()).decode()
            return f"data:image/png;base64,{img_str}"
        return None

    def validate_username(self, username):
        if username.data != self.original_user.username:
            user = db.session.scalar(
                sa.select(User).where(User.username == username.data)
            )
            if user is not None:
                raise ValidationError("Pseudo déjà utilisé.")

    def validate_email(self, email):
        if email.data != self.original_user.email:
            user = db.session.scalar(
                sa.select(User).where(User.email == email.data)
            )
            if user is not None:
                raise ValidationError(
                    "Un utilisateur existe déjà avec cette adresse email"
                )


class EditPositionForm(FlaskForm):
    name = StringField("Nom du poste", validators=[DataRequired()])
    submit = SubmitField("Valider")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, theater, position=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.theater = theater
        self.position = position
        if position:
            if self.name.data is None:
                self.name.data = position.name

    def validate_name(self, name):
        if self.position is None or name.data != self.position.name:
            position = db.session.scalar(
                sa.select(Position).where(
                    (Position.theater == self.theater)
                    & (Position.name == name.data)
                )
            )
            if position is not None:
                raise ValidationError("Un poste de même nom existe déjà")


class EditShowForm(FlaskForm):
    start = DateTimeLocalField(
        "Début",
        format='%Y-%m-%dT%H:%M',
        validators=[DataRequired()],
        widget=DateTimeLocalInput()
    )
    end = DateTimeLocalField(
        "Fin",
        format='%Y-%m-%dT%H:%M',
        validators=[DataRequired()],
        widget=DateTimeLocalInput()
    )
    film_id = SelectField("Film", coerce=int, validators=[DataRequired()])
    auditorium_id = SelectField(
        "Salle",
        coerce=lambda x: int(x) if x not in (None, '') else None,
        validators=[DataRequired()]
    )
    showtype_id = SelectField("Type de séance", coerce=lambda x: int(x) if x not in (None, '') else None)
    exclude_from_internet = BooleanField("Caché sur internet")
    submit = SubmitField("Valider")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, theater, show=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.theater = theater
        self.show = show

        # Set auditorium choices
        self.auditorium_id.choices = [
            (a.id, a.name) for a in db.session.scalars(theater.auditoriums.select())
        ]

        # Set showtype choices
        showtypes = [
            (s.id, s.name) for s in db.session.scalars(theater.showtypes.select())
        ]
        showtypes.sort(key=lambda x: x[1])
        self.showtype_id.choices = [('', '-')] + showtypes

        # Initialize film choices
        if show and show.film:
            # If editing, set the current film as the only choice initially
            self.film_id.choices = [(show.film.id, show.film.title)]
        else:
            # If creating new, start with an empty list but provide a placeholder
            self.film_id.choices = []

        if show and not self.is_submitted():
            # Only set initial values if this is not a form submission
            self.start.data = show.start
            self.end.data = show.end
            self.film_id.data = show.film_id
            self.auditorium_id.data = show.auditorium_id
            self.showtype_id.data = show.showtype_id
            self.exclude_from_internet.data = show.exclude_from_internet

    def validate_film_id(self, field):
        # Add the selected film to choices if it exists
        if field.data:
            film = db.session.get(Film, field.data)
            if film:
                # Update choices to include the selected film
                self.film_id.choices = [(film.id, film.title)]
            else:
                raise ValidationError("Film non trouvé")

    def validate_auditorium_id(self, field):
        # Get theater's auditoriums count
        auditoriums_count = db.session.scalar(
            sa.select(sa.func.count())
            .select_from(Auditorium)
            .where(Auditorium.theater_id == self.theater.id)
        )

        # If there are multiple auditoriums and none selected
        if auditoriums_count > 1 and field.data is None:
            raise ValidationError('Veuillez sélectionner une salle')

class EditTheaterDataImportForm(FlaskForm):
    cds_import_url = StringField(
        "URL d'import CDS",
        validators=[Optional(), URL(message="L'URL n'est pas valide")],
        description="URL du flux XML CDS pour l'import des séances"
    )
    submit = SubmitField("Enregistrer")


class InviteUserForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    first_name = StringField("Prénom", validators=[Optional()])
    last_name = StringField("Nom", validators=[Optional()])
    group = SelectField("Groupe", coerce=int)
    submit = SubmitField("Inviter")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, theater, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.theater = theater
        # Set group choices from theater groups
        self.group.choices = [(g.id, g.name) for g in db.session.scalars(theater.groups.select())]

    def validate_email(self, email):
        # Only check if email exists in this theater
        user = db.session.scalar(
            sa.select(User).where(
                (User.email == email.data) &
                (User.theater_id == self.theater.id)
            )
        )
        if user is not None:
            raise ValidationError("Cette adresse email est déjà utilisée dans ce cinéma")

class EditMOTDForm(FlaskForm):
    message = TextAreaField('Message', validators=[DataRequired()])
    start_date = DateTimeLocalField('Date de début',
                                  format='%Y-%m-%dT%H:%M',
                                  validators=[Optional()],
                                  widget=DateTimeLocalInput())
    end_date = DateTimeLocalField('Date de fin',
                                format='%Y-%m-%dT%H:%M',
                                validators=[Optional()],
                                widget=DateTimeLocalInput())
    target_groups = SelectMultipleField('Groupes cibles', coerce=int)
    color = RadioField('Couleur', choices=[
        ('primary', '<div class="alert alert-primary" role="alert">Message</div>'),
        ('secondary', '<div class="alert alert-secondary" role="alert">Message</div>'),
        ('success', '<div class="alert alert-success" role="alert">Message</div>'),
        ('danger', '<div class="alert alert-danger" role="alert">Message</div>'),
        ('warning', '<div class="alert alert-warning" role="alert">Message</div>'),
        ('info', '<div class="alert alert-info" role="alert">Message</div>'),
        ('light', '<div class="alert alert-light" role="alert">Message</div>'),
        ('dark', '<div class="alert alert-dark" role="alert">Message</div>')
    ], render_kw={'label_safe_html': True}, validators=[DataRequired()])
    submit = SubmitField('Valider')
    cancel = SubmitField('Annuler', render_kw={'class': 'btn btn-secondary', 'formnovalidate': True})

    def __init__(self, theater, motd=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.theater = theater
        self.target_groups.choices = sorted([(g.id, g.name) for g in db.session.scalars(theater.groups.select())], key=lambda x: x[1])
        self.motd = motd
        if motd:
            if self.message.data is None:
                self.message.data = motd.message
            if self.color.data is None:
                self.color.data = motd.color
            if self.target_groups.data is None:
                self.target_groups.data = [g.id for g in motd.get_target_groups()]
            if self.start_date.data is None:
                if motd.start_date:
                    self.start_date.data = motd.start_date.replace(tzinfo=None)
            if self.end_date.data is None:
                if motd.end_date:
                    self.end_date.data = motd.end_date.replace(tzinfo=None)
            if self.target_groups.data is None:
                self.target_groups.data = [g.id for g in motd.get_target_groups()]

    def validate_end_date(self, field):
        if not field.data:
            return

        # Convert to UTC for comparison
        end_date = field.data.replace(tzinfo=timezone.utc)
        now = datetime.now(timezone.utc)

        # If start_date is defined, end_date must be after it
        if self.start_date.data:
            start_date = self.start_date.data.replace(tzinfo=timezone.utc)
            if end_date <= start_date:
                raise ValidationError('La date de fin doit être postérieure à la date de début')

        # If no start_date, end_date must be in the future
        elif end_date <= now:
            raise ValidationError('La date de fin doit être dans le futur')
