# import re

from datetime import datetime
from jinja2 import pass_eval_context
from markupsafe import Markup, escape

from . import bp
from app.models import UserStatus


TIME_DURATION_UNITS = (
    ("semaine", 60 * 60 * 24 * 7),
    ("jour", 60 * 60 * 24),
    ("heure", 60 * 60),
    ("minute", 60),
    ("seconde", 1),
)


def human_time_duration(seconds):
    if seconds == 0:
        return "inf"
    parts = []
    for unit, div in TIME_DURATION_UNITS:
        amount, seconds = divmod(int(seconds), div)
        if amount > 0:
            parts.append(
                "{} {}{}".format(amount, unit, "" if amount == 1 else "s")
            )
    return ", ".join(parts)


@bp.app_template_filter("human_duration")
def human_duration(seconds):
    return human_time_duration(seconds)


@pass_eval_context
@bp.app_template_filter()
def newline_to_br(context, value: str) -> str:
    # Regex not working. Giving up for now
    # TODO(sbl): Fix regex
    # result = "<br />".join(re.split(r"(?:\r\n|\r|\n){2,}", escape(value)))
    result = "<br />".join(escape(value).splitlines())

    if context.autoescape:
        result = Markup(result)

    return result


@bp.app_template_filter("pluralize")
def pluralize(number, singular="", plural="s"):
    if number > 1:
        return plural
    else:
        return singular


@bp.app_template_filter("pretty_user_status")
def pretty_user_status(userstatus):
    return {
        UserStatus.ACTIVE: "actif",
        UserStatus.PENDING: "invité",
        UserStatus.DISABLED: "désactivé",
        UserStatus.BANNED: "banni",
    }.get(userstatus, "Inconnu")


@bp.app_template_filter("in_future")
def in_future(date):
    return date > datetime.now()


@bp.app_template_filter("human_date")
def human_date(date):
    return date.strftime("%A %d %B %Y<br />%H:%M")


@bp.app_template_filter("human_date_short")
def human_date_short(date):
    return date.strftime("%a %d %b, %H:%M")


@bp.app_template_filter("human_version")
def human_version(version, film):
    versions = {
        "VERSION_ORIGINAL_LOCAL": "VF (original)",
        "VERSION_LOCAL": "VF (doublé)",
        "VERSION_ORIGINAL": "VOST",
        "VERSION_MUET": "Muet",
    }
    string = versions.get(version, "Inconnu")
    if film.country and string == "VOST":
        string += f" ({film.country})"
    return string


@bp.app_template_filter("toggle")
def toggle(t, truevalue="Vrai", falsevalue="Faux"):
    return truevalue if t else falsevalue

@bp.app_template_filter("pretty_phone")
def phone(phone):
    fmt = "{}{} {}{} {}{} {}{} {}{}"
    return fmt.format(*phone)

@bp.app_template_filter("comment_type_to_string")
def comment_type_to_string(comment_type):
    return {
        "show": "séance",
        "film": "film",
    }.get(comment_type, "Inconnu")
