from flask import Blueprint

bp = Blueprint(
    "main",
    __name__,
    template_folder="templates",
    static_folder="static",
    static_url_path="/static/main",
)

from .wrapper import theater_admin_required, theater_member_required
from . import filters
from . import routes
