import datetime

from flask_login import login_required, current_user
from flask import abort, jsonify, redirect, render_template, url_for

from .. import bp
from app import db
from app.models import Show, Assignation, Position, User


@bp.route("/show")
@login_required
def show_index():
    t = current_user.theater
    shows = db.session.scalars(
        t.shows.select()
        .filter(
            Show.start >= datetime.datetime.now() - datetime.timedelta(days=7)
        )
        .order_by(Show.start.asc())
    ).all()
    return render_template(
        "shows.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        shows=shows,
        now=datetime.datetime.now(),
    )


@bp.route("/shows/mine")
@login_required
def my_shows():
    assignations = db.session.scalars(
        current_user.assignations.select().join(Assignation.show).filter(Show.start >= datetime.datetime.now() - datetime.timedelta(days=7))
    ).all()
    shows = sorted(list({a.show for a in assignations}), key=lambda s: s.start)
    return render_template(
        "shows.html.j2",
        title=f"Cinéma {current_user.theater.name}",
        theater=current_user.theater,
        shows=shows,
        now=datetime.datetime.now(),
    )


@bp.route("show/<show_id>/<position_id>/take")
@login_required
def take_show(show_id, position_id):
    try:
        t = current_user.theater
        a = Assignation(
            theater=t,
            show_id=show_id,
            position_id=position_id,
            user=current_user,
        )
        db.session.add(a)
        db.session.commit()
    except Exception as e:
        abort(500, str(e))
    return redirect(url_for("main.show_index"))


@bp.route("show/<show_id>/<position_id>/release")
@login_required
def release_show(show_id, position_id):
    t = current_user.theater
    show = db.session.scalar(t.shows.select().filter(Show.id == show_id))
    if show is None:
        abort(404, "Show not found")
    position = db.session.scalar(
        t.positions.select().filter(Position.id == position_id)
    )
    if position is None:
        abort(404, "Position not found")
    a = show.get_assignation_for(position)
    db.session.delete(a)
    db.session.commit()
    return redirect(url_for("main.show_index"))

@bp.route("show/<show_id>/<position_id>/assign/<user_id>", methods=["POST"])
@login_required
def assign_show(show_id, position_id, user_id):
    t = current_user.theater
    position = db.session.scalar(
        t.positions.select().filter(Position.id == position_id)
    )
    if position is None:
        return jsonify({"ok": False, "message": "Position not found"}), 404
    # check if current_user has ASSIGN permission on position
    if current_user.group.get_permission_for(position).name != "ASSIGN":
        return jsonify({"ok": False, "message": "You don't have permission to assign to this position"}), 403
    show = db.session.scalar(t.shows.select().filter(Show.id == show_id))
    if show is None:
        return jsonify({"ok": False, "message": "Show not found"}), 404
    if user_id == "0":
        show.remove_assignation_for(position)
        return jsonify({"ok": True})
    user = db.session.scalar(t.users.select().filter(User.id == user_id))
    if user is None:
        return jsonify({"ok": False, "message": "User not found"}), 404
    show.set_assignation(position, user)
    return jsonify({"ok": True})
