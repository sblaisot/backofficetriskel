import datetime
import sqlalchemy as sa
from datetime import timezone

from flask_login import login_required, current_user
from flask import (
    abort,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from wtforms import ValidationError

from .. import bp, theater_admin_required, theater_member_required

from ..forms import (
    EditShowtypeForm,
    EditAuditoriumForm,
    EditGroupForm,
    EditPositionForm,
    EditUserForm,
    SearchForm,
    EditShowForm,
    EditTheaterDataImportForm,
    InviteUserForm,
    EditMOTDForm,
)
from app import db, cache
from app.admin import admin_required
from app.models import (
    Assignable,
    Auditorium,
    Film,
    Group,
    Position,
    Show,
    Theater,
    User,
    UserStatus,
    Permission,
    PermissionTypes,
    ShowType,
    MOTD,
    MOTDGroup,
    ShowComment,
    ShowCommentTarget,
)
from app.utils.email import send_invitation_email


@bp.route("/theater/<int:theater_id>")
@login_required
@theater_admin_required
def theater_index(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    return render_template(
        "theater/index.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
    )


@bp.route("/theater/<int:theater_id>/auditorium")
@login_required
@theater_admin_required
def theater_auditorium(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    auditoriums = list(db.session.scalars(t.auditoriums.select()))
    return render_template(
        "theater/auditoriums.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        auditoriums=auditoriums,
    )


@bp.route("/theater/<int:theater_id>/auditorium/add", methods=["GET", "POST"])
@login_required
@theater_admin_required
def add_auditorium(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = EditAuditoriumForm(t)
    if form.cancel.data:
        return redirect(url_for("main.theater_auditorium", theater_id=t.id))
    if form.validate_on_submit():
        a = Auditorium(
            theater=t,
            name=form.name.data,
            capacity=form.capacity.data,
            cds_auditorium_number=form.cds_auditorium_number.data,
        )
        db.session.add(a)
        db.session.commit()
        return redirect(url_for("main.theater_auditorium", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        header="Ajout de salle",
        title=f"Cinéma {t.name}",
        form=form,
    )


@bp.route("/theater/<int:theater_id>/auditorium/<int:id>/delete")
@login_required
@theater_admin_required
def remove_auditorium(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    a = db.first_or_404(sa.select(Auditorium).where(Auditorium.id == id))
    db.session.delete(a)
    db.session.commit()
    return redirect(url_for("main.theater_auditorium", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/auditorium/<int:id>/edit",
    methods=["GET", "POST"],
)
@login_required
@theater_admin_required
def edit_auditorium(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    a = db.first_or_404(
        sa.select(Auditorium).where(
            (Auditorium.id == id) & (Auditorium.theater == t)
        )
    )
    form = EditAuditoriumForm(t, auditorium=a)
    if form.cancel.data:
        return redirect(url_for("main.theater_auditorium", theater_id=t.id))
    if form.validate_on_submit():
        a.name = form.name.data
        a.capacity = form.capacity.data
        a.cds_auditorium_number = form.cds_auditorium_number.data
        db.session.commit()
        return redirect(url_for("main.theater_auditorium", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header=f"Edition salle {a.name}",
        form=form,
    )


@bp.route("/theater/<int:theater_id>/showtype")
@login_required
@theater_admin_required
def theater_showtype(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    showtypes = list(db.session.scalars(t.showtypes.select()))
    return render_template(
        "theater/showtypes.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        showtypes=showtypes,
    )


@bp.route("/theater/<int:theater_id>/showtype/add", methods=["GET", "POST"])
@login_required
@theater_admin_required
def add_showtype(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = EditShowtypeForm(t)
    if form.cancel.data:
        return redirect(url_for("main.theater_showtype", theater_id=t.id))
    if form.validate_on_submit():
        s = ShowType(
            theater=t,
            name=form.name.data,
            color=form.color.data,
            description=form.description.data,
        )
        db.session.add(s)
        db.session.commit()
        return redirect(url_for("main.theater_showtype", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        header="Ajout de type de séance",
        title=f"Cinéma {t.name}",
        form=form,
    )


@bp.route("/theater/<int:theater_id>/showtype/<int:id>/delete")
@login_required
@theater_admin_required
def remove_showtype(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    s = db.first_or_404(sa.select(ShowType).where(ShowType.id == id))
    db.session.delete(s)
    db.session.commit()
    return redirect(url_for("main.theater_showtype", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/showtype/<int:id>/edit",
    methods=["GET", "POST"],
)
@login_required
@theater_admin_required
def edit_showtype(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    s = db.first_or_404(
        sa.select(ShowType).where(
            (ShowType.id == id) & (ShowType.theater == t)
        )
    )
    form = EditShowtypeForm(t, showtype=s)
    if form.cancel.data:
        return redirect(url_for("main.theater_showtype", theater_id=t.id))
    if form.validate_on_submit():
        s.name = form.name.data
        s.color = form.color.data
        s.description = form.description.data
        db.session.commit()
        return redirect(url_for("main.theater_showtype", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header=f"Edition type de séance {s.name}",
        form=form,
    )

@bp.route("/theater/<int:theater_id>/user")
@login_required
@theater_admin_required
def theater_user(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    query = request.args.get("q", None, type=str)
    page = request.args.get("page", 1, type=int)
    sqlquery = t.users.select()
    if query:
        sqlquery = sqlquery.filter(
            sa.or_(
                User.username.like(f"%{query}%"), User.email.like(f"%{query}%")
            )
        )
    users = db.paginate(sqlquery, page=page, per_page=25, error_out=False)
    next_url = (
        url_for(
            "main.theater_user",
            theater_id=t.id,
            page=users.next_num,
            q=query if query else "",
        )
        if users.has_next
        else None
    )
    prev_url = (
        url_for(
            "main.theater_user",
            theater_id=t.id,
            page=users.prev_num,
            q=query if query else "",
        )
        if users.has_prev
        else None
    )
    search_form = SearchForm()
    return render_template(
        "theater/users.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        users=users.items,
        next_url=next_url,
        prev_url=prev_url,
        pagenum=users.page,
        search_form=search_form,
    )


@bp.route("/theater/<int:theater_id>/user/<int:id>/delete")
@login_required
@theater_admin_required
def remove_user(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    if id == current_user.id:
        flash("Vous ne pouvez pas vous supprimer vous-même", "error")
        return redirect(url_for("main.theater_user", theater_id=t.id))
    u = db.first_or_404(sa.select(User).where(User.id == id))
    db.session.delete(u)
    db.session.commit()
    return redirect(url_for("main.theater_user", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/user/<int:id>/edit",
    methods=["GET", "POST"],
)
@login_required
@theater_admin_required
def edit_user(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    u = db.first_or_404(
        sa.select(User).where((User.id == id) & (User.theater == t))
    )
    form = EditUserForm(u)
    form.group.choices = [
        (g.id, g.name) for g in db.session.scalars(t.groups.select())
    ]
    if u.group is None:
        form.group.choices = [(0, "Aucun")] + form.group.choices
    if form.cancel.data:
        return redirect(url_for("main.theater_user", theater_id=t.id))
    if form.validate_on_submit():
        if form.group.data != 0:
            group = db.session.scalar(
                sa.select(Group).where(
                    (Group.id == form.group.data) & (Group.theater == t)
                )
            )
            if group is None:
                flash("Groupe inconnu", "error")
                return redirect(url_for("main.theater_user", theater_id=t.id))
            u.group = group
        u.username = form.username.data
        u.email = form.email.data
        u.first_name = form.first_name.data
        u.last_name = form.last_name.data
        u.telephone = form.telephone.data.replace(" ", "")
        u.mobile_phone = form.mobile_phone.data.replace(" ", "")
        if form.profile_picture_file.data:
            try:
                new_profile_picture = form.process_profile_picture()
                if new_profile_picture:
                    u.profile_picture = new_profile_picture
            except Exception as e:
                flash(f"Erreur lors du traitement de l'image: {str(e)}", "error")
                return redirect(url_for("main.edit_user", theater_id=t.id, id=id))
        u.status = UserStatus[form.status.data]

        db.session.commit()
        cache.delete_memoized(u.group.get_users, u.group)
        flash("Utilisateur mis à jour avec succès", "success")
        return redirect(url_for("main.theater_user", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header=f"Edition utilisateur {u.username}",
        form=form,
    )


@bp.route("/theater/<int:theater_id>/user/invite", methods=["GET", "POST"])
@login_required
@theater_admin_required
def invite_user(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = InviteUserForm(t)

    if form.cancel.data:
        return redirect(url_for("main.theater_user", theater_id=t.id))

    if form.validate_on_submit():
        # Create user in pending state
        username = f"{form.first_name.data[0].lower()}{form.last_name.data.lower()}"
        # increment username if it is already taken
        while db.session.scalar(sa.select(User).where(User.username == username)):
            if username[-1].isdigit():
                username = username[:-1] + str(int(username[-1]) + 1)
            else:
                username += "1"
        user = User(
            theater=t,
            username=username,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            email=form.email.data,
            status=UserStatus.PENDING
        )

        # Set group if selected
        if form.group.data != 0:
            group = db.session.scalar(
                sa.select(Group).where(
                    (Group.id == form.group.data) & (Group.theater == t)
                )
            )
            if group is None:
                flash("Groupe inconnu", "error")
                return redirect(url_for("main.theater_user", theater_id=t.id))
            user.group = group

        db.session.add(user)
        db.session.commit()
        cache.delete_memoized(user.group.get_users, user.group)

        # Generate token and send invitation email
        token = user.get_invitation_token()
        send_invitation_email(user, token)

        flash(f"Une invitation a été envoyée à {user.email}", "success")
        return redirect(url_for("main.theater_user", theater_id=t.id))

    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header="Inviter un utilisateur",
        form=form,
    )


@bp.route(
    "/theater/<int:theater_id>/user/<int:id>/disable",
)
@login_required
@theater_admin_required
def disable_user(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    if id == current_user.id:
        flash("Vous ne pouvez pas vous désactiver vous-même", "error")
        return redirect(url_for("main.theater_user", theater_id=t.id))
    u = db.first_or_404(
        sa.select(User).where(
            (User.id == id)
            & (User.theater == t)
            & (User.status == UserStatus.ACTIVE)
        )
    )
    u.deactivate()
    db.session.commit()
    return redirect(url_for("main.theater_user", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/user/<int:id>/activate",
)
@login_required
@theater_admin_required
def activate_user(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    u = db.first_or_404(
        sa.select(User).where(
            (User.id == id)
            & (User.theater == t)
            & (User.status == UserStatus.DISABLED)
        )
    )
    u.activate()
    db.session.commit()
    return redirect(url_for("main.theater_user", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/user/<int:id>/ban",
)
@login_required
@admin_required
def ban_user(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    u = db.first_or_404(
        sa.select(User).where((User.id == id) & (User.theater == t))
    )
    u.ban()
    db.session.commit()
    return redirect(url_for("main.theater_user", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/user/<int:id>/unban",
)
@login_required
@admin_required
def unban_user(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    u = db.first_or_404(
        sa.select(User).where((User.id == id) & (User.theater == t))
    )
    u.activate()
    db.session.commit()
    return redirect(url_for("main.theater_user", theater_id=t.id))

@bp.route('/theater/<int:theater_id>/user/<int:user_id>/mugshot')
@login_required
@theater_member_required
def user_mugshot(theater_id, user_id):
    theater = Theater.query.get_or_404(theater_id)
    user =  User.query.get_or_404(user_id)
    if user.theater != theater:
        abort(404)

    return render_template('mugshot.html.j2',
                         theater=theater,
                         user=user)

@bp.route("/theater/<int:theater_id>/group")
@login_required
@theater_admin_required
def theater_group(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    groups = list(db.session.scalars(t.groups.select()))
    return render_template(
        "theater/groups.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        groups=groups,
        dbsession=db.session,
    )


@bp.route("/theater/<int:theater_id>/group/add", methods=["GET", "POST"])
@login_required
@theater_admin_required
def add_group(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = EditGroupForm(t)
    if form.cancel.data:
        return redirect(url_for("main.theater_group", theater_id=t.id))
    if form.validate_on_submit():
        g = Group(
            theater=t,
            name=form.name.data,
            theater_admin=form.theater_admin.data,
        )
        db.session.add(g)
        db.session.commit()
        return redirect(url_for("main.theater_group", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        header="Ajout de groupe",
        title=f"Cinéma {t.name}",
        form=form,
    )


@bp.route("/theater/<int:theater_id>/group/<int:id>/delete")
@login_required
@theater_admin_required
def remove_group(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    if id == current_user.group.id:
        flash("Vous ne pouvez pas vous supprimer votre propre groupe", "error")
        return redirect(url_for("main.theater_group", theater_id=t.id))
    g = db.first_or_404(sa.select(Group).where(Group.id == id))
    db.session.delete(g)
    db.session.commit()
    return redirect(url_for("main.theater_group", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/group/<int:id>/edit",
    methods=["GET", "POST"],
)
@login_required
@theater_admin_required
def edit_group(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    g = db.first_or_404(
        sa.select(Group).where((Group.id == id) & (Group.theater == t))
    )
    form = EditGroupForm(t, group=g)
    if form.cancel.data:
        return redirect(url_for("main.theater_group", theater_id=t.id))
    if form.validate_on_submit():
        g.name = form.name.data
        g.theater_admin = form.theater_admin.data
        db.session.commit()
        cache.delete_memoized(g.get_permissions_and_assignables, g)
        return redirect(url_for("main.theater_group", theater_id=t.id))
    return render_template(
        "theater/group.html.j2",
        title=f"Cinéma {t.name}",
        group=g,
        theater=t,
        dbsession=db.session,
        form=form,
        available_permissions=[e for e in PermissionTypes],
    )


@bp.route(
    "/theater/<int:theater_id>/group/<int:id>/update_perm",
    methods=["POST"],
)
@login_required
@theater_admin_required
def update_perm(theater_id, id):
    data = request.json
    if (
        data is None
        or data.get("positionId") is None
        or type(data.get("positionId")) != int
        or data.get("value") is None
        or type(data.get("value")) != str
    ):
        abort(400, "Bad parameters")
    position_id = data.get("positionId")
    value = data.get("value")
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    g = db.first_or_404(
        sa.select(Group).where((Group.id == id) & (Group.theater == t))
    )
    p = db.first_or_404(
        sa.select(Position).where(
            (Position.id == position_id) & (Position.theater == t)
        )
    )
    perm = db.session.scalar(
        sa.select(Permission).where(
            (Permission.theater == t)
            & (Permission.group == g)
            & (Permission.position == p)
        )
    )
    if perm is None and value != "NONE":
        perm = Permission(
            theater=t,
            group=g,
            position=p,
            permission=getattr(PermissionTypes, value),
        )
        db.session.add(perm)
        db.session.commit()
        return jsonify({"ok": True})
    if value == "NONE":
        db.session.delete(perm)
        db.session.commit()
    else:
        perm.permission = getattr(PermissionTypes, value)
        db.session.commit()
    return jsonify({"ok": True})


@bp.route(
    "/theater/<int:theater_id>/group/<int:id>/assignable",
    methods=["POST"],
)
@login_required
@theater_admin_required
def update_assignable(theater_id, id):
    data = request.json
    if (
        data is None
        or data.get("positionId") is None
        or type(data.get("positionId")) != int
        or data.get("value") is None
        or type(data.get("value")) != bool
    ):
        abort(400, "Bad parameters")
    position_id = data.get("positionId")
    value = data.get("value")
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    g = db.first_or_404(
        sa.select(Group).where((Group.id == id) & (Group.theater == t))
    )
    p = db.first_or_404(
        sa.select(Position).where(
            (Position.id == position_id) & (Position.theater == t))
    )
    a = db.session.scalar(
        sa.select(Assignable).where(
            (Assignable.group == g) & (Assignable.position == p)
        )
    )
    if value and a is None:
        a = Assignable(
            group=g,
            position=p,
        )
        db.session.add(a)
        db.session.commit()
        return jsonify({"ok": True})
    if a is not None and not value:
        db.session.delete(a)
        db.session.commit()
    return jsonify({"ok": True})


@bp.route("/theater/<int:theater_id>/position")
@login_required
@theater_admin_required
def theater_position(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    positions = list(db.session.scalars(t.positions.select()))
    return render_template(
        "theater/positions.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        positions=positions,
    )


@bp.route("/theater/<int:theater_id>/position/add", methods=["GET", "POST"])
@login_required
@theater_admin_required
def add_position(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = EditPositionForm(t)
    if form.cancel.data:
        return redirect(url_for("main.theater_position", theater_id=t.id))
    if form.validate_on_submit():
        p = Position(
            theater=t,
            name=form.name.data,
        )
        db.session.add(p)
        db.session.commit()
        return redirect(url_for("main.theater_position", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        header="Ajout de poste",
        title=f"Cinéma {t.name}",
        form=form,
    )


@bp.route("/theater/<int:theater_id>/position/<int:id>/delete")
@login_required
@theater_admin_required
def remove_position(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    p = db.first_or_404(sa.select(Position).where(Position.id == id))
    db.session.delete(p)
    db.session.commit()
    return redirect(url_for("main.theater_position", theater_id=t.id))


@bp.route(
    "/theater/<int:theater_id>/position/<int:id>/edit",
    methods=["GET", "POST"],
)
@login_required
@theater_admin_required
def edit_position(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    p = db.first_or_404(
        sa.select(Position).where(
            (Position.id == id) & (Position.theater == t)
        )
    )
    form = EditPositionForm(t, position=p)
    if form.cancel.data:
        return redirect(url_for("main.theater_position", theater_id=t.id))
    if form.validate_on_submit():
        p.name = form.name.data
        db.session.commit()
        return redirect(url_for("main.theater_position", theater_id=t.id))
    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header=f"Edition poste {p.name}",
        form=form,
    )


@bp.route('/theater/<int:theater_id>/mugshots')
@login_required
@theater_member_required
def mugshots(theater_id):
    theater = Theater.query.get_or_404(theater_id)
    users = db.session.scalars(theater.users.select()).all()
    positions = set(position for position in db.session.scalars(theater.positions.select()))

    return render_template('mugshots.html.j2',
                         theater=theater,
                         users=users,
                         positions=positions)


@bp.route("/theater/<int:theater_id>/show")
@login_required
@theater_admin_required
def theater_show(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    query = request.args.get("q", None, type=str)
    page = request.args.get("page", 1, type=int)
    sqlquery = t.shows.select().filter(
        Show.start >= datetime.datetime.now() - datetime.timedelta(days=7)
    )
    if query:
        sqlquery = sqlquery.join(Show.film).filter(
            Film.title.like(f"%{query}%"),
        )
    sqlquery = sqlquery.order_by(Show.start.asc())
    shows = db.paginate(sqlquery, page=page, per_page=25, error_out=False)
    next_url = (
        url_for(
            "main.theater_show",
            theater_id=t.id,
            page=shows.next_num,
            q=query if query else "",
        )
        if shows.has_next
        else None
    )
    prev_url = (
        url_for(
            "main.theater_show",
            theater_id=t.id,
            page=shows.prev_num,
            q=query if query else "",
        )
        if shows.has_prev
        else None
    )
    search_form = SearchForm()
    return render_template(
        "theater/shows.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        shows=shows.items,
        now=datetime.datetime.now(),
        next_url=next_url,
        prev_url=prev_url,
        pagenum=shows.page,
        search_form=search_form,
    )


@bp.route("/theater/<int:theater_id>/show/<int:id>/edit", methods=["GET", "POST"])
@login_required
@theater_admin_required
def edit_show(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    show = db.first_or_404(
        sa.select(Show).where(
            (Show.id == id) & (Show.theater == t)
        )
    )

    form = EditShowForm(t, show=show)

    if form.cancel.data:
        return redirect(url_for("main.theater_show", theater_id=t.id))

    if form.is_submitted():
        # Call validate_film_id before form validation
        try:
            form.validate_film_id(form.film_id)
        except ValidationError as e:
            form.film_id.errors = list(form.film_id.errors) + [str(e)]
            return render_template(
                "theater/edit_show.html.j2",
                title=f"Cinéma {t.name}",
                show=show,
                header="Edition séance",
                form=form,
                theater=t,
            )

        if form.validate():
            show.start = form.start.data
            show.end = form.end.data
            show.film_id = form.film_id.data
            show.auditorium_id = form.auditorium_id.data
            show.showtype_id = form.showtype_id.data
            show.exclude_from_internet = form.exclude_from_internet.data

            db.session.commit()
            flash("Séance mise à jour avec succès", "success")
            return redirect(url_for("main.theater_show", theater_id=t.id))

    return render_template(
        "theater/edit_show.html.j2",
        title=f"Cinéma {t.name}",
        header="Edition séance",
        form=form,
        theater=t,
        show=show,
    )


@bp.route("/theater/<int:theater_id>/film/autocomplete")
@login_required
@theater_admin_required
def film_autocomplete(theater_id):
    search = request.args.get('q', '')
    if len(search) < 2:
        return jsonify([])

    films = db.session.scalars(
        sa.select(Film)
        .where(Film.title.ilike(f"%{search}%"))
        .limit(10)
    ).all()

    return jsonify([{
        'id': film.id,
        'text': film.title
    } for film in films])


@bp.route("/theater/<int:theater_id>/show/<int:id>/delete")
@login_required
@theater_admin_required
def delete_show(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    show = db.first_or_404(
        sa.select(Show).where(
            (Show.id == id) & (Show.theater == t)
        )
    )

    db.session.delete(show)
    db.session.commit()
    flash("Séance supprimée avec succès", "success")
    return redirect(url_for("main.theater_show", theater_id=t.id))


@bp.route("/theater/<int:theater_id>/show/add", methods=["GET", "POST"])
@login_required
@theater_admin_required
def add_show(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))

    form = EditShowForm(t)

    # If theater has multiple auditoriums, prepend an empty choice
    auditoriums = list(db.session.scalars(t.auditoriums.select()))
    if len(auditoriums) > 1:
        form.auditorium_id.choices = [(None, '-- Sélectionner une salle --')] + [
            (a.id, a.name) for a in auditoriums
        ]

    if form.cancel.data:
        return redirect(url_for("main.theater_show", theater_id=t.id))

    if form.is_submitted():
        # Call validate_film_id before form validation
        try:
            form.validate_film_id(form.film_id)
        except ValidationError as e:
            form.film_id.errors = list(form.film_id.errors) + [str(e)]
            return render_template(
                "theater/edit_show.html.j2",
                title=f"Cinéma {t.name}",
                header="Nouvelle séance",
                form=form,
                theater=t,
            )

        if form.validate():
            show = Show(
                theater=t,
                start=form.start.data,
                end=form.end.data,
                film_id=form.film_id.data,
                auditorium_id=form.auditorium_id.data,
                exclude_from_internet=form.exclude_from_internet.data
            )

            db.session.add(show)
            db.session.commit()
            flash("Séance créée avec succès", "success")
            return redirect(url_for("main.theater_show", theater_id=t.id))

    return render_template(
        "theater/edit_show.html.j2",
        title=f"Cinéma {t.name}",
        header="Nouvelle séance",
        form=form,
        theater=t,
    )


@bp.route("/theater/<int:theater_id>/data_import", methods=["GET", "POST"])
@login_required
@theater_admin_required
def theater_data_import(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = EditTheaterDataImportForm()

    if form.validate_on_submit():
        t.cds_import_url = form.cds_import_url.data
        db.session.commit()
        flash("URL d'import mise à jour avec succès", "success")
        return redirect(url_for("main.theater_data_import", theater_id=t.id))

    elif request.method == "GET":
        form.cds_import_url.data = t.cds_import_url

    # Get import log from session if it exists
    import_log = session.pop('import_log', None)

    return render_template(
        "theater/data_import.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        form=form,
        import_log=import_log,
    )

@bp.route("/theater/<int:theater_id>/data_import/run")
@login_required
@theater_admin_required
def run_data_import(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))

    if not t.cds_import_url:
        flash("Aucune URL d'import configurée", "error")
        return redirect(url_for("main.theater_data_import", theater_id=t.id))

    try:
        import_log = t.import_from_cds()
        if not import_log:  # If no changes were made
            flash("Import terminé - Aucune modification nécessaire", "info")
        else:
            # Store import log in session to display it after redirect
            session['import_log'] = import_log
            flash("Import terminé avec succès", "success")
    except Exception as e:
        flash(f"Erreur lors de l'import: {str(e)}", "error")

    return redirect(url_for("main.theater_data_import", theater_id=t.id))

@bp.route("/theater/<int:theater_id>/motd")
@login_required
@theater_admin_required
def theater_motd(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    motds = list(db.session.scalars(t.motds.select()))
    return render_template(
        "theater/motd.html.j2",
        title=f"Cinéma {t.name}",
        theater=t,
        motds=motds,
    )

@bp.route("/theater/<int:theater_id>/motd/add", methods=["GET", "POST"])
@login_required
@theater_admin_required
def add_motd(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    form = EditMOTDForm(t)

    if form.cancel.data:
        return redirect(url_for("main.theater_motd", theater_id=t.id))

    if form.validate_on_submit():
        # Convert form dates to UTC timezone
        start_date = form.start_date.data.replace(tzinfo=timezone.utc) if form.start_date.data else None
        end_date = form.end_date.data.replace(tzinfo=timezone.utc) if form.end_date.data else None

        motd = MOTD(
            theater=t,
            message=form.message.data,
            color=form.color.data,
            start_date=start_date,
            end_date=end_date
        )
        db.session.add(motd)

        # Add target groups
        if form.target_groups.data:
            for group_id in form.target_groups.data:
                group = db.session.get(Group, group_id)
                if group and group.theater == t:
                    mg = MOTDGroup(motd=motd, group=group)
                    db.session.add(mg)

        db.session.commit()
        flash("Message ajouté avec succès", "success")
        return redirect(url_for("main.theater_motd", theater_id=t.id))

    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header="Nouveau message",
        form=form,
    )

@bp.route("/theater/<int:theater_id>/motd/<int:id>/edit", methods=["GET", "POST"])
@login_required
@theater_admin_required
def edit_motd(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    motd = db.first_or_404(
        sa.select(MOTD).where(
            (MOTD.id == id) & (MOTD.theater == t)
        )
    )
    form = EditMOTDForm(t, motd=motd)

    if form.cancel.data:
        return redirect(url_for("main.theater_motd", theater_id=t.id))

    if form.validate_on_submit():
        print("Form data:", {
            'message': form.message.data,
            'color': form.color.data,
            'start_date': form.start_date.data,
            'end_date': form.end_date.data,
            'target_groups': form.target_groups.data
        })
        print(form.__dict__)
        # Convert form dates to UTC timezone
        start_date = form.start_date.data.replace(tzinfo=timezone.utc) if form.start_date.data else None
        end_date = form.end_date.data.replace(tzinfo=timezone.utc) if form.end_date.data else None

        motd.message = form.message.data
        motd.color = form.color.data
        motd.start_date = start_date
        motd.end_date = end_date
        # Update target groups
        db.session.execute(motd.target_groups.delete())
        if form.target_groups.data:
            for group_id in form.target_groups.data:
                group = db.session.get(Group, group_id)
                if group and group.theater == t:
                    mg = MOTDGroup(motd=motd, group=group)
                    db.session.add(mg)

        db.session.commit()
        flash("Message mis à jour avec succès", "success")
        return redirect(url_for("main.theater_motd", theater_id=t.id))

    return render_template(
        "simple_form.html.j2",
        title=f"Cinéma {t.name}",
        header=f"Edition message",
        form=form,
    )

@bp.route("/theater/<int:theater_id>/motd/<int:id>/delete")
@login_required
@theater_admin_required
def remove_motd(theater_id, id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    motd = db.first_or_404(
        sa.select(MOTD).where(
            (MOTD.id == id) & (MOTD.theater == t)
        )
    )
    db.session.delete(motd)
    db.session.commit()
    flash("Message supprimé avec succès", "success")
    return redirect(url_for("main.theater_motd", theater_id=t.id))

@bp.route("/theater/<int:theater_id>/motd/cleanup")
@login_required
@theater_admin_required
def cleanup_motd(theater_id):
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))

    # Get all expired MOTDs
    expired_motds = [motd for motd in db.session.scalars(t.motds.select()) if motd.is_expired()]

    if not expired_motds:
        flash("Aucun message expiré à supprimer", "info")
        return redirect(url_for("main.theater_motd", theater_id=t.id))

    # Delete all expired MOTDs
    count = len(expired_motds)
    for motd in expired_motds:
        db.session.delete(motd)

    db.session.commit()
    flash(f"{count} message{'s' if count > 1 else ''} expiré{'s' if count > 1 else ''} supprimé{'s' if count > 1 else ''}", "success")
    return redirect(url_for("main.theater_motd", theater_id=t.id))

@bp.route("/theater/<int:theater_id>/show/<int:show_id>/comment/add", methods=["POST"])
@login_required
@theater_admin_required
def add_show_comment(theater_id, show_id):
    """Add a comment to a show"""
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    show = db.first_or_404(
        sa.select(Show).where(
            (Show.id == show_id) & (Show.theater == t)
        )
    )

    # Create comment
    comment = ShowComment(
        message=request.form.get("message"),
        author=current_user,
        show=show
    )
    db.session.add(comment)

    # Add targets
    target_groups = request.form.getlist("target_groups")
    target_positions = request.form.getlist("target_positions")

    for group_id in target_groups:
        group = db.session.get(Group, int(group_id))
        if group and group.theater == t:
            target = ShowCommentTarget(comment=comment, group=group)
            db.session.add(target)

    for position_id in target_positions:
        position = db.session.get(Position, int(position_id))
        if position and position.theater == t:
            target = ShowCommentTarget(comment=comment, position=position)
            db.session.add(target)

    db.session.commit()
    cache.delete_memoized(show.get_all_comments, show)
    cache.delete_memoized(show.get_comments_for_user, show)
    return jsonify({"ok": True})


@bp.route("/theater/<int:theater_id>/show/<int:show_id>/comment/<int:comment_id>", methods=["DELETE"])
@login_required
@theater_member_required
def delete_show_comment(theater_id, show_id, comment_id):
    """Delete a comment from a show"""
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    show = db.first_or_404(
        sa.select(Show).where(
            (Show.id == show_id) & (Show.theater == t)
        )
    )
    comment = db.first_or_404(
        sa.select(ShowComment).where(
            (ShowComment.id == comment_id) &
            (ShowComment.show == show)
        )
    )

    # Check permissions
    if not current_user.is_theater_admin(t) and comment.author != current_user:
        return jsonify({
            "ok": False,
            "message": "Vous n'avez pas le droit de supprimer ce commentaire"
        }), 403

    db.session.delete(comment)
    db.session.commit()
    cache.delete_memoized(show.get_all_comments, show)
    cache.delete_memoized(show.get_comments_for_user, show)
    return jsonify({"ok": True})

@bp.route("/theater/<int:theater_id>/show/<int:show_id>/comment/<int:comment_id>/edit", methods=["POST"])
@login_required
@theater_member_required
def edit_show_comment(theater_id, show_id, comment_id):
    """Edit a show comment"""
    t = db.first_or_404(sa.select(Theater).where(Theater.id == theater_id))
    show = db.first_or_404(
        sa.select(Show).where(
            (Show.id == show_id) & (Show.theater == t)
        )
    )
    comment = db.first_or_404(
        sa.select(ShowComment).where(
            (ShowComment.id == comment_id) &
            (ShowComment.show == show)
        )
    )

    # Check permissions
    if not current_user.is_theater_admin(t) and comment.author != current_user:
        return jsonify({
            "ok": False,
            "message": "Vous n'avez pas le droit de modifier ce commentaire"
        }), 403

    # Update comment
    comment.message = request.form.get("message")

    # Update targets
    db.session.execute(comment.targets.delete())

    target_groups = request.form.getlist("target_groups")
    target_positions = request.form.getlist("target_positions")

    for group_id in target_groups:
        group = db.session.get(Group, int(group_id))
        if group and group.theater == t:
            target = ShowCommentTarget(comment=comment, group=group)
            db.session.add(target)

    for position_id in target_positions:
        position = db.session.get(Position, int(position_id))
        if position and position.theater == t:
            target = ShowCommentTarget(comment=comment, position=position)
            db.session.add(target)

    db.session.commit()
    cache.delete_memoized(show.get_all_comments, show)
    cache.delete_memoized(show.get_comments_for_user, show)
    return jsonify({"ok": True})

