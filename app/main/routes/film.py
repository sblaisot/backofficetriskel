import sqlalchemy as sa

from flask_login import login_required
from flask import abort, render_template, request, url_for

from .. import bp
from ..forms import SearchForm
from app import db
from app.models import Film


@bp.route("/film/<id>")
@login_required
def fiche_film(id):
    f = db.session.scalar(sa.select(Film).where(Film.id == id))
    if f is None:
        abort(404, "Film non trouvé")
    return render_template("film.html.j2", title=f.title, film=f)


@bp.route("/film", methods=["GET", "POST"])
@login_required
def liste_films():
    query = request.args.get("q", None, type=str)
    page = request.args.get("page", 1, type=int)
    sqlquery = sa.select(Film)
    if query:
        sqlquery = sqlquery.filter(Film.title.like(f"%{query}%"))
    sqlquery = sqlquery.order_by(Film.id.desc())
    films = db.paginate(sqlquery, page=page, per_page=25, error_out=False)
    next_url = (
        url_for(
            "main.liste_films", page=films.next_num, q=query if query else ""
        )
        if films.has_next
        else None
    )
    prev_url = (
        url_for(
            "main.liste_films", page=films.prev_num, q=query if query else ""
        )
        if films.has_prev
        else None
    )
    search_form = SearchForm()
    return render_template(
        "films.html.j2",
        title="Films",
        films=films.items,
        next_url=next_url,
        prev_url=prev_url,
        pagenum=films.page,
        search_form=search_form,
    )
