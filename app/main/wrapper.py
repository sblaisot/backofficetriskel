import sqlalchemy as sa

from flask import abort, request
from flask_login import current_user
from functools import wraps

from app import db
from app.models import Theater


def theater_admin_required(func):
    """
    If you decorate a view with this, it will ensure that the current user has
    admin privileges. For example:
        @app.route('/post')
        @login_required
        @admin_required
        def post():
            pass
    """

    @wraps(func)
    def wrapped(*args, **kwargs):
        theater_id = request.view_args.get("theater_id", None)
        if theater_id is None:
            abort(
                500,
                "@theater_admin_required wrapper missing theater_id parameter.",
            )
        t = Theater.query.get_or_404(theater_id)
        if not current_user.is_theater_admin(t):
            abort(403, "Vous n'êtes pas autorisé à accéder à cette URL.")
        return func(*args, **kwargs)

    return wrapped


def theater_member_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        theater_id = request.view_args.get("theater_id", None)
        if theater_id is None:
            abort(
                500,
                "@theater_member_required wrapper missing theater_id parameter.",
            )
        theater = Theater.query.get_or_404(theater_id)
        if theater != current_user.theater:
            abort(403)  # Forbidden
        return func(*args, **kwargs)
    return decorated_function
