from flask import current_app, render_template

from app.utils.email import send_email


def send_password_reset_email(user):
    token = user.get_reset_password_token()
    send_email(
        "[Ciné] Réinitialisation de votre mot de passe",
        sender=current_app.config["MAIL_FROM"],
        recipients=[user.email],
        text_body=render_template(
            "email/reset_password.txt.j2", user=user, token=token
        ),
        html_body=render_template(
            "email/reset_password.html.j2", user=user, token=token
        ),
    )


def send_mail_validation_link(user, new_email):
    token = user.get_temporary_token(
        data={"user_id": user.id, "new_email": new_email}
    )
    send_email(
        "[Ciné] Mise à jour de votre email",
        sender=current_app.config["MAIL_FROM"],
        recipients=[new_email],
        text_body=render_template(
            "email/update_email.txt.j2",
            user=user,
            token=token,
            new_email=new_email,
        ),
        html_body=render_template(
            "email/update_email.html.j2",
            user=user,
            token=token,
            new_email=new_email,
        ),
    )
