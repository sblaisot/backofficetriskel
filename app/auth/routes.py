import sqlalchemy as sa

from urllib.parse import urlsplit
from flask import abort, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_user, logout_user, login_required

from . import bp
from .email import (
    send_password_reset_email,
    send_mail_validation_link,
)
from .forms import (
    LoginForm,
    ResetPasswordRequestForm,
    ResetPasswordForm,
    UpdatePasswordForm,
    UpdateEmailRequestForm,
    ProfileForm,
)
from app import db
from app.models import User, UserStatus


@bp.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = db.session.scalar(
            sa.select(User).where(User.username == form.username.data)
        )
        if user is None or not user.check_password(form.password.data):
            flash("Pseudo ou mot de passe invalide", "error")
            return redirect(url_for("auth.login"))
        if user.status != UserStatus.ACTIVE:
            flash("Utilisateur désactivé", "error")
            return redirect(url_for("auth.login"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")
        if not next_page or urlsplit(next_page).netloc != "":
            next_page = url_for("main.index")
        return redirect(next_page)
    return render_template("auth/login.html.j2", title="Sign In", form=form)


@bp.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("main.index"))


@bp.route("/profile", methods=["GET", "POST"])
@login_required
def profile():
    form = ProfileForm(current_user)
    if form.cancel.data:
        return redirect(url_for("main.index"))
    if form.validate_on_submit():
        current_user.telephone = form.telephone.data.replace(" ", "")
        current_user.mobile_phone = form.mobile_phone.data.replace(" ", "")
        if form.profile_picture_file.data:
            try:
                new_profile_picture = form.process_profile_picture()
                if new_profile_picture:
                    current_user.profile_picture = new_profile_picture
            except Exception as e:
                flash(f"Erreur lors du traitement de l'image: {str(e)}", "error")
                return redirect(url_for("auth.profile"))
        db.session.commit()
        flash("Vos informations ont été mises à jour.")
        return redirect(url_for("auth.profile"))

    return render_template("auth/profile.html.j2", title="Profile", form=form)


@bp.route("/reset_password_request", methods=["GET", "POST"])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = db.session.scalar(
            sa.select(User).where(
                (User.email == form.email.data)
                & (User.status == UserStatus.ACTIVE)
            )
        )
        if user:
            send_password_reset_email(user)
        flash(
            "Consultez les instructions envoyées par mail pour réinitialiser votre mot de passe"
        )
        return redirect(url_for("auth.login"))
    return render_template(
        "auth/reset_password_request.html.j2",
        title="Reset Password",
        form=form,
    )


@bp.route("/reset_password/<token>", methods=["GET", "POST"])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for("main.index"))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        if form.password.data != form.password2.data:
            flash("Les 2 nouveaux mots de passe ne correspondent pas", "error")
            return redirect(url_for("update_password"))
        user.set_password(form.password.data)
        db.session.commit()
        flash("Votre mot de passe a été mis à jour.")
        return redirect(url_for("auth.login"))
    return render_template("auth/reset_password.html.j2", form=form)


@bp.route("/update_password", methods=["GET", "POST"])
def update_password():
    form = UpdatePasswordForm()
    if form.validate_on_submit():
        if not current_user.check_password(form.current_password.data):
            flash("Mot de passe actuel invalide", "error")
            return redirect(url_for("auth.update_password"))
        if form.new_password.data != form.new_password2.data:
            flash("Les 2 nouveaux mots de passe ne correspondent pas", "error")
            return redirect(url_for("auth.update_password"))
        current_user.set_password(form.new_password.data)
        db.session.commit()
        flash("Votre mot de passe a été mis à jour.")
        return redirect(url_for("auth.profile"))
    return render_template("auth/update_password.html.j2", form=form)


@bp.route("/update_email_request", methods=["GET", "POST"])
@login_required
def update_email_request():
    form = UpdateEmailRequestForm()
    if form.validate_on_submit():
        send_mail_validation_link(current_user, form.email.data)
        flash(
            "Consultez les instructions envoyées par mail pour valider la modification de votre adresse email"
        )
        return redirect(url_for("auth.profile"))
    return render_template(
        "auth/update_email_request.html.j2", title="Update email", form=form
    )


@bp.route("/update_email/<token>")
@login_required
def update_email(token):
    data = User.verify_token(token)
    if data.get("user_id") != current_user.id:
        abort(
            403,
            "Ce lien de changement d'email n'est pas lié à votre utilisateur",
        )
    new_email = data.get("new_email")
    if not new_email:
        abort(400, "Jeton invalide")
    current_user.email = new_email
    db.session.commit()
    flash("Votre adresse email a été mise à jour.")
    return redirect(url_for("auth.profile"))


@bp.route("/validate_invitation/<token>", methods=["GET", "POST"])
def validate_invitation(token):
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))

    user = User.verify_invitation_token(token)
    if not user or user.status != UserStatus.PENDING:
        flash("Le lien d'invitation n'est pas valide ou a expiré", "error")
        return redirect(url_for("main.index"))

    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        user.status = UserStatus.ACTIVE
        db.session.commit()

        flash("Votre compte a été activé avec succès.", "success")
        return redirect(url_for("auth.login"))

    return render_template(
        "auth/validate_invitation.html.j2",
        title="Valider l'invitation",
        form=form
    )
