from flask_login import current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import DataRequired, Email, EqualTo, Optional, Length, Regexp, Email, ValidationError
import base64
from PIL import Image
from io import BytesIO

class LoginForm(FlaskForm):
    username = StringField("Pseudo", validators=[DataRequired()])
    password = PasswordField("Mot de passe", validators=[DataRequired()])
    remember_me = BooleanField("Se souvenir de moi")
    submit = SubmitField("Connexion")


class ResetPasswordRequestForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    submit = SubmitField("Demander la réinitialisation")


class ResetPasswordForm(FlaskForm):
    password = PasswordField("Mot de passe", validators=[DataRequired()])
    password2 = PasswordField(
        "Répétez votre mot de passe",
        validators=[DataRequired(), EqualTo("password", 'Les mots de passe ne correspondent pas')],
    )
    submit = SubmitField("Valider")


class UpdatePasswordForm(FlaskForm):
    current_password = PasswordField(
        "Mot de passe courant", validators=[DataRequired()]
    )
    new_password = PasswordField(
        "Nouveau mot de passe", validators=[DataRequired()]
    )
    new_password2 = PasswordField(
        "Répétez le nouveau mot de passe",
        validators=[DataRequired(), EqualTo("new_password", 'Les mots de passe ne correspondent pas')],
    )
    submit = SubmitField("Valider")

    def validate_current_password(self, field):
        if not current_user.check_password(field.data):
            raise ValidationError("Mot de passe actuel invalide")

    def validate_new_password(self, field):
        if field.data == self.current_password.data:
            raise ValidationError("Le nouveau mot de passe ne peut pas être identique au mot de passe actuel")

class UpdateEmailRequestForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    submit = SubmitField("Mettre à jour mon email")

class ProfileForm(FlaskForm):
    telephone = StringField('Téléphone fixe', validators=[Optional(), Length(min=10, max=14), Regexp(regex='^[0-9 ]+$')])
    mobile_phone = StringField("Téléphone mobile", validators=[Optional(), Length(min=10, max=14), Regexp(regex='^[0-9 ]+$')])
    profile_picture_file = FileField("Photo de profil", validators=[
        Optional(),
        FileAllowed(['jpg', 'jpeg', 'png'], 'Images uniquement!')
    ])
    submit = SubmitField("Valider")
    cancel = SubmitField(
        label="Annuler",
        render_kw={"formnovalidate": True, "class": "btn-secondary"},
    )

    def __init__(self, original_user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.original_user = original_user
        if original_user:
            if self.telephone.data is None:
                self.telephone.data = original_user.telephone
            if self.mobile_phone.data is None:
                self.mobile_phone.data = original_user.mobile_phone

    def process_profile_picture(self):
        """Process the uploaded profile picture and return base64 encoded string"""
        if self.profile_picture_file.data:
            # Read the uploaded file
            image = Image.open(self.profile_picture_file.data)

            # Resize image to a reasonable size (e.g., 200x200)
            image.thumbnail((200, 200))

            # Save the image to a bytes buffer
            buffer = BytesIO()
            image.save(buffer, format="PNG")

            # Encode as base64
            img_str = base64.b64encode(buffer.getvalue()).decode()
            return f"data:image/png;base64,{img_str}"
        return None
