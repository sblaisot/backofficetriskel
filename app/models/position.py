import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db, cache


class Position(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    name: so.Mapped[str] = so.mapped_column(sa.String(256), nullable=False)
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )

    theater: so.Mapped["Theater"] = so.relationship(back_populates="positions")
    permissions: so.WriteOnlyMapped["Permission"] = so.relationship(
        back_populates="position",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    assignations: so.WriteOnlyMapped["Assignation"] = so.relationship(
        back_populates="position",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    assignables: so.WriteOnlyMapped["Assignable"] = so.relationship(
        back_populates="position",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<Position {self.name}>"

    @cache.memoize(3)
    def get_assignable_users(self):
        users = set()
        assignables = db.session.scalars(self.assignables.select())
        for a in assignables:
            for user in db.session.scalars(a.group.users.select()):
                users.add(user)
        return sorted(list(users), key=lambda x: x.full_name)
