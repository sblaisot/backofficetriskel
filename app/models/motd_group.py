import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db


class MOTDGroup(db.Model):
    """Association table for MOTD-Group many-to-many relationship"""
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    motd_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("motd.id", ondelete="CASCADE"), index=True
    )
    group_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("group.id", ondelete="CASCADE"), index=True
    )

    motd: so.Mapped["MOTD"] = so.relationship(back_populates="target_groups")
    group: so.Mapped["Group"] = so.relationship(back_populates="motd_targets")
