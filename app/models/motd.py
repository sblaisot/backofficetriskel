import sqlalchemy as sa
import sqlalchemy.orm as so
from datetime import datetime, timezone
from typing import Optional
from app import db


class MOTD(db.Model):
    """Message of the Day model"""
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    message: so.Mapped[str] = so.mapped_column(sa.Text, nullable=False)
    color: so.Mapped[str] = so.mapped_column(sa.String(32), nullable=False)
    start_date: so.Mapped[Optional[datetime]] = so.mapped_column(
        default=lambda: datetime.now(timezone.utc)
    )
    end_date: so.Mapped[Optional[datetime]] = so.mapped_column(nullable=True)
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )

    theater: so.Mapped["Theater"] = so.relationship(back_populates="motds")
    target_groups: so.WriteOnlyMapped["MOTDGroup"] = so.relationship(
        back_populates="motd",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<MOTD {self.message[:20]}... for {self.theater.name}>"

    def is_active(self):
        """Check if the message should be currently displayed"""
        now = datetime.now().replace(tzinfo=timezone.utc)

        # Convert naive datetimes to UTC if needed
        start_date = self.start_date
        if start_date and start_date.tzinfo is None:
            start_date = start_date.replace(tzinfo=timezone.utc)

        end_date = self.end_date
        if end_date and end_date.tzinfo is None:
            end_date = end_date.replace(tzinfo=timezone.utc)

        if start_date and now < start_date:
            return False
        if end_date and now >= end_date:
            return False
        return True

    def is_future(self):
        """Check if the message is scheduled to be displayed in the future"""
        now = datetime.now().replace(tzinfo=timezone.utc)
        # Convert naive datetimes to UTC if needed
        start_date = self.start_date
        if start_date and start_date.tzinfo is None:
            start_date = start_date.replace(tzinfo=timezone.utc)
        return start_date and start_date > now

    def is_expired(self):
        """Check if the message is expired"""
        now = datetime.now().replace(tzinfo=timezone.utc)
        # Convert naive datetimes to UTC if needed
        end_date = self.end_date
        if end_date and end_date.tzinfo is None:
            end_date = end_date.replace(tzinfo=timezone.utc)
        return end_date and end_date < now

    def get_target_groups(self):
        """Get list of groups this message targets"""
        return [mg.group for mg in db.session.scalars(self.target_groups.select())]

    def should_show_to(self, user):
        """Check if this message should be shown to given user"""
        if not self.is_active():
            return False
        if not user.group:
            return False
        target_groups = self.get_target_groups()
        return len(target_groups) == 0 or user.group in target_groups
