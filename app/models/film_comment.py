import sqlalchemy as sa
import sqlalchemy.orm as so
from datetime import datetime, timezone
from typing import Optional
from app import db, cache


class FilmCommentTarget(db.Model):
    """Association table for FilmComment targets (groups or positions)"""
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    comment_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("film_comment.id", ondelete="CASCADE"), index=True
    )
    group_id: so.Mapped[Optional[int]] = so.mapped_column(
        sa.ForeignKey("group.id", ondelete="CASCADE"), index=True, nullable=True
    )
    position_id: so.Mapped[Optional[int]] = so.mapped_column(
        sa.ForeignKey("position.id", ondelete="CASCADE"), index=True, nullable=True
    )

    comment: so.Mapped["FilmComment"] = so.relationship(back_populates="targets")
    group: so.Mapped[Optional["Group"]] = so.relationship()
    position: so.Mapped[Optional["Position"]] = so.relationship()


class FilmComment(db.Model):
    """Comments on films that can be targeted to specific groups/positions"""
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    message: so.Mapped[str] = so.mapped_column(sa.Text, nullable=False)
    created_at: so.Mapped[datetime] = so.mapped_column(
        default=lambda: datetime.now(timezone.utc)
    )
    author_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("user.id", ondelete="SET NULL"), nullable=True
    )
    film_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("film.id", ondelete="CASCADE"), index=True
    )
    comment_type: str = "film"

    author: so.Mapped[Optional["User"]] = so.relationship()
    film: so.Mapped["Film"] = so.relationship(back_populates="comments")
    targets: so.WriteOnlyMapped["FilmCommentTarget"] = so.relationship(
        back_populates="comment",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<FilmComment {self.message[:20]}... for {self.film}>"

    @cache.memoize(15)
    def get_target_groups(self):
        """Get list of groups this comment targets"""
        return [
            target.group
            for target in db.session.scalars(
                self.targets.select().where(FilmCommentTarget.group_id.is_not(None))
            )
        ]

    def get_target_groups_names(self):
        """Get list of groups this comment targets"""
        return [
            g.name
            for g in self.get_target_groups()
        ]

    @cache.memoize(15)
    def get_target_positions(self):
        """Get list of positions this comment targets"""
        return [
            target.position
            for target in db.session.scalars(
                self.targets.select().where(FilmCommentTarget.position_id.is_not(None))
            )
        ]

    def get_target_positions_names(self):
        """Get list of positions this comment targets"""
        return [
            p.name
            for p in self.get_target_positions()
        ]

    def should_show_to(self, user):
        """Check if this comment should be shown to given user"""
        if not user.group:
            return False

        target_groups = self.get_target_groups()
        target_positions = self.get_target_positions()

        # If no targets specified, show to everyone
        if not target_groups and not target_positions:
            return True

        # Check if user's group is targeted
        if user.group in target_groups:
            return True

        # Check if user can be assigned to any targeted position
        user_positions = set(user.get_assignable_positions())
        return bool(user_positions.intersection(target_positions))
