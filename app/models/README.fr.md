# Documentation du Modèle de Données

Ce document décrit le modèle de données utilisé dans l'application, y compris les différentes tables et leurs relations.

## Tables

### Theater (Cinéma)

- Représente un cinéma
- Champs : id, name (nom), city (ville)
- Relations :
  - possede plusieurs Users (Utilisateurs)
  - possede plusieurs Groups (Groupes)
  - possede plusieurs Permissions
  - possede plusieurs Auditoriums (Salles)
  - possede plusieurs Positions (Postes)
  - possede plusieurs Shows (Séances)
  - possede plusieurs Assignations (Affectations)
  - possede plusieurs MOTDs (Messages du jour)

```mermaid
erDiagram
    Theater ||--o{ User : possede
    Theater ||--o{ Group : possede
    Theater ||--o{ Permission : possede
    Theater ||--o{ Auditorium : possede
    Theater ||--o{ Position : possede
    Theater ||--o{ Show : possede
    Theater ||--o{ Assignation : possede
    Theater ||--o{ MOTD : possede
```

### Auditorium (Salle)

- Représente une salle dans un cinéma
- Champs : id, name (nom), capacity (capacité), cds_auditorium_number, theater_id
- Relations :
  - Appartient à un Theater
  - possede plusieurs Shows

```mermaid
erDiagram
    Auditorium }|--|| Theater : appartient_a
    Auditorium ||--o{ Show : possede
```

### User (Utilisateur)

- Représente un utilisateur dans le système
- Champs : id, username (nom d'utilisateur), email, password_hash, siteAdmin, status, theater_id, group_id
- Relations :
  - Appartient à un Theater
  - Appartient à un Group (optionnel)
  - possede plusieurs Assignations

```mermaid
erDiagram
    User }|--|| Theater : appartient_a
    User }o--|| Group : appartient_a
    User ||--o{ Assignation : possede
```

### Group (Groupe)

- Représente un groupe d'utilisateurs dans un cinéma
- Champs : id, name (nom), theater_id, theater_admin
- Relations :
  - Appartient à un Theater
  - possede plusieurs Users
  - possede plusieurs Permissions
  - possede plusieurs Assignables

```mermaid
erDiagram
    Group }|--|| Theater : appartient_a
    Group ||--o{ User : possede
    Group ||--o{ Permission : possede
    Group ||--o{ Assignable : possede
```

### Position (Poste)

- Représente un poste dans un cinéma
- Champs : id, name (nom), theater_id
- Relations :
  - Appartient à un Theater
  - possede plusieurs Permissions
  - possede plusieurs Assignations
  - possede plusieurs Assignables

```mermaid
erDiagram
    Position }|--|| Theater : appartient_a
    Position ||--o{ Permission : possede
    Position ||--o{ Assignation : possede
    Position ||--o{ Assignable : possede
```

### Permission

- Représente les permissions d'un groupe sur un poste
- Un des types suivants :
  - None : le groupe ne peut pas voir l'affectation du poste
  - Read : le groupe peut voir l'affectation mais ne peut pas la modifier
  - Take : le groupe peut prendre et libérer l'affectation pour lui-même mais ne peut pas
          affecter quelqu'un d'autre
  - Assign : le groupe peut affecter quelqu'un à ce poste et libérer
            le poste même s'il est pris par quelqu'un d'autre
- Champs : id, theater_id, group_id, position_id, permission
- Relations :
  - Appartient à un Theater
  - Appartient à un Group
  - Appartient à une Position

```mermaid
erDiagram
    Permission }|--|| Theater : appartient_a
    Permission }|--|| Group : appartient_a
    Permission }|--|| Position : appartient_a
```

### Assignable

- Représente quel groupe peut être affecté à un poste
- Champs : id, position_id, group_id
- Relations :
  - Appartient à une Position
  - Appartient à un Group

```mermaid
erDiagram
    Assignable }|--|| Position : appartient_a
    Assignable }|--|| Group : appartient_a
```

### Film

- Représente un film
- Champs : id, title (titre), storyline (synopsis), rating (classification), visa, cds_id, allocine_id, trailerUrl, posterUrl, country (pays), genre, director (réalisateur), cast (distribution), duration (durée)
- Relations :
  - possede plusieurs Shows
  - possede plusieurs FilmComments

```mermaid
erDiagram
    Film ||--o{ Show : possede
    Film ||--o{ FilmComment : possede
```

### Show (Séance)

- Représente une séance
- Champs : id, start (début), end (fin), exclude_from_internet, cds_id, theater_id, auditorium_id, film_id, film_depth, film_audio, film_version
- Relations :
  - Appartient à un Theater
  - Appartient à un Auditorium
  - Appartient à un Film
  - possede plusieurs Assignations
  - possede plusieurs ShowComments

```mermaid
erDiagram
    Show }|--|| Theater : appartient_a
    Show }|--|| Auditorium : appartient_a
    Show }|--|| Film : appartient_a
    Show ||--o{ Assignation : possede
    Show ||--o{ ShowComment : possede
```

### Assignation (Affectation)

- Représente l'affectation d'un utilisateur à un poste pour une séance spécifique
- Champs : id, theater_id, show_id, position_id, user_id
- Relations :
  - Appartient à un Theater
  - Appartient à un Show
  - Appartient à une Position
  - Appartient à un User

```mermaid
erDiagram
    Assignation }|--|| Theater : appartient_a
    Assignation }|--|| Show : appartient_a
    Assignation }|--|| Position : appartient_a
    Assignation }|--|| User : appartient_a
```

### ShowComment (Commentaire de séance)

- Représente un commentaire sur une séance qui peut être ciblé vers des groupes/postes spécifiques
- Champs : id, message, created_at (date de création), author_id (auteur), show_id
- Relations :
  - Appartient à un Show
  - Appartient à un User (auteur)
  - possede plusieurs ShowCommentTargets

```mermaid
erDiagram
    ShowComment }|--|| Show : appartient_a
    ShowComment }o--|| User : appartient_a
    ShowComment ||--o{ ShowCommentTarget : possede
```

### ShowCommentTarget (Cible de commentaire de séance)

- Table d'association pour les cibles des commentaires de séance (groupes ou postes)
- Champs : id, comment_id, group_id (optionnel), position_id (optionnel)
- Relations :
  - Appartient à un ShowComment
  - Appartient optionnellement à un Group
  - Appartient optionnellement à une Position

```mermaid
erDiagram
    ShowCommentTarget }|--|| ShowComment : appartient_a
    ShowCommentTarget }o--|| Group : appartient_a
    ShowCommentTarget }o--|| Position : appartient_a
```

### FilmComment (Commentaire de film)

- Représente un commentaire sur un film qui peut être ciblé vers des groupes/postes spécifiques
- Champs : id, message, created_at (date de création), author_id (auteur), film_id
- Relations :
  - Appartient à un Film
  - Appartient à un User (auteur)
  - possede plusieurs FilmCommentTargets

```mermaid
erDiagram
    FilmComment }|--|| Film : appartient_a
    FilmComment }o--|| User : appartient_a
    FilmComment ||--o{ FilmCommentTarget : possede
```

### FilmCommentTarget (Cible de commentaire de film)

- Table d'association pour les cibles des commentaires de film (groupes ou postes)
- Champs : id, comment_id, group_id (optionnel), position_id (optionnel)
- Relations :
  - Appartient à un FilmComment
  - Appartient optionnellement à un Group
  - Appartient optionnellement à une Position

```mermaid
erDiagram
    FilmCommentTarget }|--|| FilmComment : appartient_a
    FilmCommentTarget }o--|| Group : appartient_a
    FilmCommentTarget }o--|| Position : appartient_a
```

### MOTD (Message du jour)

- Représente un message qui peut être affiché à des groupes spécifiques
- Champs : id, message, color (couleur), start_date (date de début), end_date (date de fin), theater_id
- Relations :
  - Appartient à un Theater
  - possede plusieurs MOTDGroups

```mermaid
erDiagram
    MOTD }|--|| Theater : appartient_a
    MOTD ||--o{ MOTDGroup : possede
```

### MOTDGroup (Association MOTD-Groupe)

- Table d'association pour la relation plusieurs-à-plusieurs entre MOTD et Group
- Champs : id, motd_id, group_id
- Relations :
  - Appartient à un MOTD
  - Appartient à un Group

```mermaid
erDiagram
    MOTDGroup }|--|| MOTD : appartient_a
    MOTDGroup }|--|| Group : appartient_a
```

## Vue d'ensemble des Relations

1. Un Theater possede plusieurs Users, Groups, Permissions, Auditoriums, Positions, Shows et Assignations.
2. Un User appartient à un Theater et optionnellement à un Group.
3. Un Group appartient à un Theater et possede plusieurs Users, Permissions et Assignables.
4. Une Position appartient à un Theater et possede plusieurs Permissions, Assignations et Assignables.
5. Une Permission lie un Group, une Position et un Theater.
6. Un Assignable lie une Position et un Group.
7. Un Auditorium appartient à un Theater et possede plusieurs Shows.
8. Un Film possede plusieurs Shows.
9. Un Show appartient à un Theater, un Auditorium, un Film et possede plusieurs Assignations.
10. Une Assignation lie un Theater, un Show, une Position et un User.
11. Un Film possede plusieurs FilmComments
12. Un Show possede plusieurs ShowComments
13. Un ShowComment appartient à un Show et un User (auteur)
14. Un FilmComment appartient à un Film et un User (auteur)
15. Un MOTD appartient à un Theater et cible plusieurs Groups via MOTDGroup
16. Les commentaires (Show et Film) peuvent cibler des Groups et Positions spécifiques via leurs tables de cibles respectives

## Représentation du Schéma

```mermaid
erDiagram
Theater ||--o{ User : possede
Theater ||--o{ Group : possede
Theater ||--o{ Permission : possede
Theater ||--o{ Auditorium : possede
Theater ||--o{ Position : possede
Theater ||--o{ Show : possede
Theater ||--o{ Assignation : possede
Theater ||--o{ MOTD : possede
User }|--|| Theater : appartient_a
User }o--|| Group : appartient_a
User ||--o{ Assignation : possede
Group }|--|| Theater : appartient_a
Group ||--o{ User : possede
Group ||--o{ Permission : possede
Group ||--o{ Assignable : possede
Position }|--|| Theater : appartient_a
Position ||--o{ Permission : possede
Position ||--o{ Assignation : possede
Position ||--o{ Assignable : possede
Permission }|--|| Theater : appartient_a
Permission }|--|| Group : appartient_a
Permission }|--|| Position : appartient_a
Assignable }|--|| Position : appartient_a
Assignable }|--|| Group : appartient_a
Auditorium }|--|| Theater : appartient_a
Auditorium ||--o{ Show : possede
Film ||--o{ Show : possede
Show }|--|| Theater : appartient_a
Show }|--|| Auditorium : appartient_a
Show }|--|| Film : appartient_a
Show ||--o{ Assignation : possede
Assignation }|--|| Theater : appartient_a
Assignation }|--|| Show : appartient_a
Assignation }|--|| Position : appartient_a
Assignation }|--|| User : appartient_a
Film ||--o{ FilmComment : possede
Show ||--o{ ShowComment : possede
ShowComment }|--|| Show : appartient_a
ShowComment }o--|| User : appartient_a
ShowComment ||--o{ ShowCommentTarget : possede
FilmComment }|--|| Film : appartient_a
FilmComment }o--|| User : appartient_a
FilmComment ||--o{ FilmCommentTarget : possede
MOTD }|--|| Theater : appartient_a
MOTD ||--o{ MOTDGroup : possede
```
