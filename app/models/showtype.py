import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db


class ShowType(db.Model):
    __tablename__ = 'showtype'
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    name: so.Mapped[str] = so.mapped_column(sa.String(64))
    color: so.Mapped[str] = so.mapped_column(sa.String(64))
    description: so.Mapped[str] = so.mapped_column(sa.Text, nullable=True)
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )

    theater: so.Mapped["Theater"] = so.relationship(back_populates="showtypes")
    shows: so.WriteOnlyMapped["Show"] = so.relationship(
        back_populates="showtype",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
