import sqlalchemy as sa
import sqlalchemy.orm as so
from datetime import datetime, timezone
from typing import Optional
from app import db, cache
from app.models import Assignation


class Show(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    start: so.Mapped[datetime] = so.mapped_column(
        default=lambda: datetime.now(timezone.utc)
    )
    end: so.Mapped[Optional[datetime]] = so.mapped_column(
        default=lambda: datetime.now(timezone.utc)
    )
    exclude_from_internet: so.Mapped[bool] = so.mapped_column(default=False)
    cds_id: so.Mapped[int] = so.mapped_column(index=True, nullable=True)
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )
    auditorium_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("auditorium.id", ondelete="CASCADE"), index=True
    )
    film_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("film.id", ondelete="CASCADE"), index=True
    )
    film_depth: so.Mapped[str] = so.mapped_column(sa.String(64), nullable=True)
    film_audio: so.Mapped[str] = so.mapped_column(sa.String(64), nullable=True)
    film_version: so.Mapped[str] = so.mapped_column(
        sa.String(64), nullable=True
    )
    showtype_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("showtype.id", ondelete="SET NULL"), index=True, nullable=True
    )

    theater: so.Mapped["Theater"] = so.relationship(back_populates="shows")
    auditorium: so.Mapped["Auditorium"] = so.relationship(
        back_populates="shows"
    )
    film: so.Mapped["Film"] = so.relationship(back_populates="shows")
    assignations: so.WriteOnlyMapped["Assignation"] = so.relationship(
        back_populates="show",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    showtype: so.Mapped["ShowType"] = so.relationship(back_populates="shows")
    comments: so.WriteOnlyMapped["ShowComment"] = so.relationship(
        back_populates="show",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<Show {self.film.title} on {self.start}>"

    def get_assignations(self):
        return db.session.scalars(self.assignations.select()).all()

    def get_assignations_by_position_name(self):
        return {
            a.position.name: a
            for a in db.session.scalars(self.assignations.select()).all()
        }

    def get_assignation_for(self, position):
        return db.session.scalar(
            self.assignations.select().where(
                Assignation.position_id == position.id
            )
        )

    def set_assignation(self, position, user):
        a = self.get_assignation_for(position)
        if a is None:
            a = Assignation(theater=self.theater, show=self, position=position, user=user)
        a.user = user
        db.session.add(a)
        db.session.commit()

    def remove_assignation_for(self, position):
        a = self.get_assignation_for(position)
        if a is not None:
            db.session.delete(a)
            db.session.commit()

    @cache.memoize(15)
    def get_comments_for_user(self, user):
        """Get comments that should be shown to given user"""
        return [
            comment for comment in db.session.scalars(self.comments.select())
            if comment.should_show_to(user)
        ]

    @cache.memoize(15)
    def get_all_comments(self):
        """Get all comments for this show"""
        return db.session.scalars(self.comments.select()).all()
