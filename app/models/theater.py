import requests
import xmltodict
import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db, cache
from app.utils.cds import insert_shows_from_cds


class Theater(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    name: so.Mapped[str] = so.mapped_column(sa.String(1024))
    city: so.Mapped[str] = so.mapped_column(sa.String(256), index=True)
    cds_import_url: so.Mapped[str] = so.mapped_column(sa.String(1024), nullable=True)

    users: so.WriteOnlyMapped["User"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    groups: so.WriteOnlyMapped["Group"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    permissions: so.WriteOnlyMapped["Permission"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    auditoriums: so.WriteOnlyMapped["Auditorium"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    positions: so.WriteOnlyMapped["Position"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    shows: so.WriteOnlyMapped["Show"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    assignations: so.WriteOnlyMapped["Assignation"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    showtypes: so.WriteOnlyMapped["ShowType"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    motds: so.WriteOnlyMapped["MOTD"] = so.relationship(
        back_populates="theater",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<Theater {self.name} in {self.city}>"

    @cache.memoize(5)
    def get_positions(self):
        return db.session.scalars(self.positions.select()).all()

    def get_admin_users(self):
        from app.models import User, Group
        return db.session.scalars(
            self.users.select()
            .join(User.group)
            .filter(Group.theater_admin == True)
        ).all()

    def import_from_cds(self, verbose=False):
        if self.cds_import_url is None:
            return []
        cds_data = requests.get(self.cds_import_url)
        shows = xmltodict.parse(cds_data.text)["Shows"]["Show"]
        return insert_shows_from_cds(self, shows, verbose=verbose)

    def get_active_motds(self):
        """Get all currently active MOTDs for this theater"""
        return [
            motd for motd in db.session.scalars(self.motds.select())
            if motd.is_active()
        ]

    def get_motds_for_user(self, user):
        """Get active MOTDs that should be shown to given user"""
        return [
            motd for motd in self.get_active_motds()
            if motd.should_show_to(user)
        ]

    def get_groups(self):
        """Get list of groups for this theater"""
        return list(db.session.scalars(self.groups.select()))
