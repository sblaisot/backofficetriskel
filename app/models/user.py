import enum
import jwt
import sqlalchemy as sa
import sqlalchemy.orm as so

from flask import current_app
from flask_login import UserMixin
from time import time
from typing import Optional
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login
from app.models import Theater


class UserStatus(enum.Enum):
    PENDING = "pending"
    ACTIVE = "active"
    DISABLED = "disabled"
    BANNED = "banned"


class User(UserMixin, db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    username: so.Mapped[str] = so.mapped_column(
        sa.String(64), index=True, unique=True
    )
    email: so.Mapped[str] = so.mapped_column(
        sa.String(120), index=True, unique=True
    )
    password_hash: so.Mapped[Optional[str]] = so.mapped_column(sa.String(256))
    siteAdmin: so.Mapped[bool] = so.mapped_column(default=False)
    status: so.Mapped[UserStatus]
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"),
        index=True,
    )
    group_id: so.Mapped[Optional[int]] = so.mapped_column(
        sa.ForeignKey("group.id", ondelete="SET NULL"),
        index=True,
    )
    first_name: so.Mapped[Optional[str]] = so.mapped_column(sa.String(64))
    last_name: so.Mapped[Optional[str]] = so.mapped_column(sa.String(64))
    telephone: so.Mapped[Optional[str]] = so.mapped_column(sa.String(20))
    mobile_phone: so.Mapped[Optional[str]] = so.mapped_column(sa.String(20))
    profile_picture: so.Mapped[Optional[str]] = so.mapped_column(sa.Text(10000000))

    theater: so.Mapped["Theater"] = so.relationship(back_populates="users")
    group: so.Mapped[Optional["Group"]] = so.relationship(
        back_populates="users"
    )
    assignations: so.WriteOnlyMapped["Assignation"] = so.relationship(
        back_populates="user",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    @property
    def full_name(self):
        if self.first_name and self.last_name:
            return f"{self.first_name} {self.last_name}"
        return self.username

    def __repr__(self):
        return "<User {}>".format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def activate(self):
        self.status = UserStatus.ACTIVE

    def deactivate(self):
        self.status = UserStatus.DISABLED

    def ban(self):
        self.status = UserStatus.BANNED

    def get_reset_password_token(self, expires_in=600):
        return self.get_temporary_token(
            {"reset_password": self.id}, expires_in=expires_in
        )

    def get_assignable_positions(self):
        return sorted(set(a.position for a in db.session.scalars(self.group.assignables.select())), key=lambda p: p.name)

    def get_assignable_positions_names(self):
        return [p.name for p in self.get_assignable_positions()]

    def get_temporary_token(self, data, expires_in=600):
        token = {"exp": time() + expires_in}
        if data:
            token.update(data)
        return jwt.encode(
            token,
            current_app.config["SECRET_KEY"],
            algorithm="HS256",
        )

    def is_theater_admin(self, t: Theater):
        if self.siteAdmin:
            return True
        if self.theater != t:
            return False
        if self.group is None or self.group.theater != t:
            return False
        return self.group.theater_admin

    @staticmethod
    def verify_reset_password_token(token):
        try:
            user_id = jwt.decode(
                token, current_app.config["SECRET_KEY"], algorithms=["HS256"]
            )["reset_password"]
        except Exception:
            return
        return db.session.get(User, user_id)

    @staticmethod
    def verify_token(token):
        try:
            data = jwt.decode(
                token, current_app.config["SECRET_KEY"], algorithms=["HS256"]
            )
        except Exception:
            return
        return data

    def get_invitation_token(self, expires_in=7*24*3600):
        return jwt.encode(
            {
                'invite_user_id': self.id,
                'exp': time() + expires_in
            },
            current_app.config['SECRET_KEY'],
            algorithm='HS256'
        )

    @staticmethod
    def verify_invitation_token(token):
        try:
            id = jwt.decode(
                token,
                current_app.config['SECRET_KEY'],
                algorithms=['HS256']
            )['invite_user_id']
        except:
            return None
        return db.session.scalar(sa.select(User).where(User.id == id))


@login.user_loader
def load_user(id):
    u = db.session.get(User, int(id))
    return u if u.status == UserStatus.ACTIVE else None
