import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db, cache
from app.models import Permission, PermissionTypes, Assignable


class Group(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    name: so.Mapped[str] = so.mapped_column(sa.String(256), nullable=False)
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )
    theater_admin: so.Mapped[bool] = so.mapped_column(default=False)

    theater: so.Mapped["Theater"] = so.relationship(back_populates="groups")
    users: so.WriteOnlyMapped["User"] = so.relationship(
        back_populates="group",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    permissions: so.WriteOnlyMapped["Permission"] = so.relationship(
        back_populates="group",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    assignables: so.WriteOnlyMapped["Assignable"] = so.relationship(
        back_populates="group",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    motd_targets: so.WriteOnlyMapped["MOTDGroup"] = so.relationship(
        back_populates="group",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<Group {self.name} for theater {self.theater.name}>"

    @cache.memoize(15)
    def get_users(self):
        users = db.session.scalars(self.users.select())
        return list(users)

    @cache.memoize(15)
    def get_permissions_and_assignables(self):
        positions = db.session.scalars(self.theater.positions.select())
        data = {}
        for position in positions:
            assignable = db.session.scalar(
                self.assignables.select().where(
                    Assignable.position == position
                )
            )
            data[position] = {"assignable": assignable is not None}

            if self.theater_admin:
                data[position]["perm"] = Permission(
                    theater=self.theater,
                    group=self,
                    position=position,
                    permission=PermissionTypes.ASSIGN,
                )
                continue
            perm = db.session.scalar(
                self.permissions.select().where(
                    Permission.position == position
                )
            )
            if perm is None:
                perm = Permission(
                    theater=self.theater,
                    group=self,
                    position=position,
                    permission=PermissionTypes.NONE,
                )
            data[position]["perm"] = perm
        return data

    @cache.memoize(15)
    def get_permission_for(self, position):
        if self.theater_admin:
            return PermissionTypes.ASSIGN
        perm = db.session.scalar(
            self.permissions.select().where(Permission.position == position)
        )
        if perm is None:
            return PermissionTypes.NONE
        return perm.permission

    def set_permissions(self, position, permission):
        perm = db.session.scalar(
            self.permissions.select().where(Permission.position == position)
        )
        if perm is None:
            perm = Permission(
                theater=self.theater,
                group=self,
                position=position,
            )
            db.session.add(perm)
        perm.permission = permission
        db.session.commit()
