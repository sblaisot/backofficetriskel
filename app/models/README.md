# Data Model Documentation

This document describes the data model used in the application, including the various tables and their relationships.

## Tables

### Theater

- Represents a theater
- Fields: id, name, city
- Relationships:
  - Has many Users
  - Has many Groups
  - Has many Permissions
  - Has many Auditoriums
  - Has many Positions
  - Has many Shows
  - Has many Assignations
  - Has many MOTDs

```mermaid
erDiagram
    Theater ||--o{ User : has
    Theater ||--o{ Group : has
    Theater ||--o{ Permission : has
    Theater ||--o{ Auditorium : has
    Theater ||--o{ Position : has
    Theater ||--o{ Show : has
    Theater ||--o{ Assignation : has
    Theater ||--o{ MOTD : has
```

### Auditorium

- Represents an auditorium within a theater
- Fields: id, name, capacity, cds_auditorium_number, theater_id
- Relationships:
  - Belongs to one Theater
  - Has many Shows

```mermaid
erDiagram
    Auditorium }|--|| Theater : belongs_to
    Auditorium ||--o{ Show : has
```

### User

- Represents a user in the system
- Fields: id, username, email, password_hash, siteAdmin, status, theater_id, group_id
- Relationships:
  - Belongs to one Theater
  - Belongs to one Group (optional)
  - Has many Assignations

```mermaid
erDiagram
    User }|--|| Theater : belongs_to
    User }o--|| Group : belongs_to
    User ||--o{ Assignation : has
```

### Group

- Represents a group of users within a theater
- Fields: id, name, theater_id, theater_admin
- Relationships:
  - Belongs to one Theater
  - Has many Users
  - Has many Permissions
  - Has many Assignables

```mermaid
erDiagram
    Group }|--|| Theater : belongs_to
    Group ||--o{ User : has
    Group ||--o{ Permission : has
    Group ||--o{ Assignable : has
```

### Position

- Represents a position within a theater
- Fields: id, name, theater_id
- Relationships:
  - Belongs to one Theater
  - Has many Permissions
  - Has many Assignations
  - Has many Assignables

```mermaid
erDiagram
    Position }|--|| Theater : belongs_to
    Position ||--o{ Permission : has
    Position ||--o{ Assignation : has
    Position ||--o{ Assignable : has
```

### Permission

- Represents permissions for a group on a position
- One of:
  - None: group can't see position assignment
  - Read: group can see position assignment but cannot modify it
  - Take: group can take and release assignment itself but cannot
          assign someone else
  - Assign: group can assign someone on this position and release
            position even if take by someone else
- Fields: id, theater_id, group_id, position_id, permission
- Relationships:
  - Belongs to one Theater
  - Belongs to one Group
  - Belongs to one Position

```mermaid
erDiagram
    Permission }|--|| Theater : belongs_to
    Permission }|--|| Group : belongs_to
    Permission }|--|| Position : belongs_to
```

### Assignable

- Represents which group can be assigned to a position
- Fields: id, position_id, group_id
- Relationships:
  - Belongs to one Position
  - Belongs to one Group

```mermaid
erDiagram
    Assignable }|--|| Position : belongs_to
    Assignable }|--|| Group : belongs_to
```

### Film

- Represents a film
- Fields: id, title, storyline, rating, visa, cds_id, allocine_id, trailerUrl, posterUrl, country, genre, director, cast, duration
- Relationships:
  - Has many Shows
  - Has many FilmComments

```mermaid
erDiagram
    Film ||--o{ Show : has
    Film ||--o{ FilmComment : has
```

### Show

- Represents a show
- Fields: id, start, end, exclude_from_internet, cds_id, theater_id, auditorium_id, film_id, film_depth, film_audio, film_version
- Relationships:
  - Belongs to one Theater
  - Belongs to one Auditorium
  - Belongs to one Film
  - Has many Assignations
  - Has many ShowComments

```mermaid
erDiagram
    Show }|--|| Theater : belongs_to
    Show }|--|| Auditorium : belongs_to
    Show }|--|| Film : belongs_to
    Show ||--o{ Assignation : has
    Show ||--o{ ShowComment : has
```

### Assignation

- Represents the assignment of a user to a position for a specific show
- Fields: id, theater_id, show_id, position_id, user_id
- Relationships:
  - Belongs to one Theater
  - Belongs to one Show
  - Belongs to one Position
  - Belongs to one User

```mermaid
erDiagram
    Assignation }|--|| Theater : belongs_to
    Assignation }|--|| Show : belongs_to
    Assignation }|--|| Position : belongs_to
    Assignation }|--|| User : belongs_to
```

### ShowComment

- Represents a comment on a show that can be targeted to specific groups/positions
- Fields: id, message, created_at, author_id, show_id
- Relationships:
  - Belongs to one Show
  - Belongs to one User (author)
  - Has many ShowCommentTargets

```mermaid
erDiagram
    ShowComment }|--|| Show : belongs_to
    ShowComment }o--|| User : belongs_to
    ShowComment ||--o{ ShowCommentTarget : has
```

### ShowCommentTarget

- Association table for ShowComment targets (groups or positions)
- Fields: id, comment_id, group_id (optional), position_id (optional)
- Relationships:
  - Belongs to one ShowComment
  - Optionally belongs to one Group
  - Optionally belongs to one Position

```mermaid
erDiagram
    ShowCommentTarget }|--|| ShowComment : belongs_to
    ShowCommentTarget }o--|| Group : belongs_to
    ShowCommentTarget }o--|| Position : belongs_to
```

### FilmComment

- Represents a comment on a film that can be targeted to specific groups/positions
- Fields: id, message, created_at, author_id, film_id
- Relationships:
  - Belongs to one Film
  - Belongs to one User (author)
  - Has many FilmCommentTargets

```mermaid
erDiagram
    FilmComment }|--|| Film : belongs_to
    FilmComment }o--|| User : belongs_to
    FilmComment ||--o{ FilmCommentTarget : has
```

### FilmCommentTarget

- Association table for FilmComment targets (groups or positions)
- Fields: id, comment_id, group_id (optional), position_id (optional)
- Relationships:
  - Belongs to one FilmComment
  - Optionally belongs to one Group
  - Optionally belongs to one Position

```mermaid
erDiagram
    FilmCommentTarget }|--|| FilmComment : belongs_to
    FilmCommentTarget }o--|| Group : belongs_to
    FilmCommentTarget }o--|| Position : belongs_to
```

### MOTD (Message of the Day)

- Represents a message that can be displayed to specific groups
- Fields: id, message, color, start_date, end_date, theater_id
- Relationships:
  - Belongs to one Theater
  - Has many MOTDGroups

```mermaid
erDiagram
    MOTD }|--|| Theater : belongs_to
    MOTD ||--o{ MOTDGroup : has
```

### MOTDGroup

- Association table for MOTD-Group many-to-many relationship
- Fields: id, motd_id, group_id
- Relationships:
  - Belongs to one MOTD
  - Belongs to one Group

```mermaid
erDiagram
    MOTDGroup }|--|| MOTD : belongs_to
    MOTDGroup }|--|| Group : belongs_to
```

## Relationships Overview

1. A Theater has many Users, Groups, Permissions, Auditoriums, Positions, Shows, and Assignations.
2. A User belongs to one Theater and optionally to one Group.
3. A Group belongs to one Theater and has many Users, Permissions, and Assignables.
4. A Position belongs to one Theater and has many Permissions, Assignations, and Assignables.
5. A Permission links a Group, a Position, and a Theater.
6. An Assignable links a Position and a Group.
7. An Auditorium belongs to one Theater and has many Shows.
8. A Film has many Shows.
9. A Show belongs to one Theater, one Auditorium, one Film, and has many Assignations.
10. An Assignation links a Theater, a Show, a Position, and a User.
11. A Film has many FilmComments
12. A Show has many ShowComments
13. A ShowComment belongs to one Show and one User (author)
14. A FilmComment belongs to one Film and one User (author)
15. A MOTD belongs to one Theater and targets many Groups through MOTDGroup
16. Comments (Show and Film) can target specific Groups and Positions through their respective target tables

## Schema Representation

```mermaid
erDiagram
Theater ||--o{ User : has
Theater ||--o{ Group : has
Theater ||--o{ Permission : has
Theater ||--o{ Auditorium : has
Theater ||--o{ Position : has
Theater ||--o{ Show : has
Theater ||--o{ Assignation : has
Theater ||--o{ MOTD : has
User }|--|| Theater : belongs_to
User }o--|| Group : belongs_to
User ||--o{ Assignation : has
Group }|--|| Theater : belongs_to
Group ||--o{ User : has
Group ||--o{ Permission : has
Group ||--o{ Assignable : has
Position }|--|| Theater : belongs_to
Position ||--o{ Permission : has
Position ||--o{ Assignation : has
Position ||--o{ Assignable : has
Permission }|--|| Theater : belongs_to
Permission }|--|| Group : belongs_to
Permission }|--|| Position : belongs_to
Assignable }|--|| Position : belongs_to
Assignable }|--|| Group : belongs_to
Auditorium }|--|| Theater : belongs_to
Auditorium ||--o{ Show : has
Film ||--o{ Show : has
Film ||--o{ FilmComment : has
Show }|--|| Theater : belongs_to
Show }|--|| Auditorium : belongs_to
Show }|--|| Film : belongs_to
Show ||--o{ Assignation : has
Assignation }|--|| Theater : belongs_to
Assignation }|--|| Show : belongs_to
Assignation }|--|| Position : belongs_to
Assignation }|--|| User : belongs_to
ShowComment }|--|| Show : belongs_to
ShowComment }o--|| User : belongs_to
ShowComment ||--o{ ShowCommentTarget : has
FilmComment }|--|| Film : belongs_to
FilmComment }o--|| User : belongs_to
FilmComment ||--o{ FilmCommentTarget : has
MOTD }|--|| Theater : belongs_to
MOTD ||--o{ MOTDGroup : has
```
