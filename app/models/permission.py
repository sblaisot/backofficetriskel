import enum
import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db


class PermissionTypes(enum.Enum):
    NONE = "Aucune"
    READ = "Voir"
    TAKE = "Se positionner"
    ASSIGN = "Assigner"


class Permission(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )
    group_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("group.id", ondelete="CASCADE"), index=True
    )
    position_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("position.id", ondelete="CASCADE"), index=True
    )
    permission: so.Mapped[PermissionTypes]

    theater: so.Mapped["Theater"] = so.relationship(
        back_populates="permissions"
    )
    group: so.Mapped["Group"] = so.relationship(back_populates="permissions")
    position: so.Mapped["Position"] = so.relationship(
        back_populates="permissions"
    )

    def __repr__(self):
        return f"<Permission {self.permission.value} on position {self.position.name} for theater {self.theater.name}, group {self.group.name}>"
