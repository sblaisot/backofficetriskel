import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db


class Auditorium(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    name: so.Mapped[str] = so.mapped_column(sa.String(256))
    capacity: so.Mapped[int] = so.mapped_column(nullable=False)
    cds_auditorium_number: so.Mapped[int] = so.mapped_column(
        nullable=True, index=True
    )
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )

    theater: so.Mapped["Theater"] = so.relationship(
        back_populates="auditoriums"
    )
    shows: so.WriteOnlyMapped["Show"] = so.relationship(
        back_populates="auditorium",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<Auditorium {self.name} for {self.theater.name} in {self.theater.city}>"
