import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db


class Film(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    title: so.Mapped[str] = so.mapped_column(
        sa.String(256), nullable=False, index=True
    )
    storyline: so.Mapped[str] = so.mapped_column(sa.Text, nullable=True)
    rating: so.Mapped[str] = so.mapped_column(sa.String(64), nullable=True)
    visa: so.Mapped[str] = so.mapped_column(sa.String(64), nullable=True)
    cds_id: so.Mapped[int] = so.mapped_column(index=True, nullable=True)
    allocine_id: so.Mapped[str] = so.mapped_column(
        sa.String(64), nullable=True
    )
    trailerUrl: so.Mapped[str] = so.mapped_column(
        sa.String(256), nullable=True
    )
    posterUrl: so.Mapped[str] = so.mapped_column(sa.String(256), nullable=True)
    country: so.Mapped[str] = so.mapped_column(sa.String(64), nullable=True)
    genre: so.Mapped[str] = so.mapped_column(sa.String(64), nullable=True)
    director: so.Mapped[str] = so.mapped_column(sa.String(64), nullable=True)
    cast: so.Mapped[str] = so.mapped_column(sa.String(256), nullable=True)
    duration: so.Mapped[int] = so.mapped_column()

    shows: so.WriteOnlyMapped["Show"] = so.relationship(
        back_populates="film",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    comments: so.WriteOnlyMapped["FilmComment"] = so.relationship(
        back_populates="film",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<Film {self.title}>"

    def get_comments_for_user(self, user):
        """Get comments that should be shown to given user"""
        return [
            comment for comment in db.session.scalars(self.comments.select())
            if comment.should_show_to(user)
        ]

    def get_all_comments(self):
        """Get all comments for this show"""
        return db.session.scalars(self.comments.select()).all()
