import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db


class Assignable(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    position_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("position.id", ondelete="CASCADE"), index=True
    )
    group_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("group.id", ondelete="CASCADE"), index=True
    )

    position: so.Mapped["Position"] = so.relationship(
        back_populates="assignables"
    )
    group: so.Mapped["Group"] = so.relationship(back_populates="assignables")

    def __repr__(self):
        return (
            f"<Assignable {self.group.name} on position {self.position.name}>"
        )
