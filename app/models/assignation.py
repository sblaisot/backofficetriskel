import sqlalchemy as sa
import sqlalchemy.orm as so
from app import db


class Assignation(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    theater_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("theater.id", ondelete="CASCADE"), index=True
    )
    show_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("show.id", ondelete="CASCADE"), index=True
    )
    position_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("position.id", ondelete="CASCADE"), index=True
    )
    user_id: so.Mapped[int] = so.mapped_column(
        sa.ForeignKey("user.id", ondelete="CASCADE"), index=True
    )

    theater: so.Mapped["Theater"] = so.relationship(
        back_populates="assignations"
    )
    show: so.Mapped["Show"] = so.relationship(back_populates="assignations")
    position: so.Mapped["Position"] = so.relationship(
        back_populates="assignations"
    )
    user: so.Mapped["User"] = so.relationship(back_populates="assignations")

    def __repr__(self):
        return f"<Assignation {self.user.username} on position {self.position.name} for show {self.show}>"
