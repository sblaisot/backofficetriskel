"""Move attributes from film to show

Revision ID: 6b729746663f
Revises: d8f0e6f9066a
Create Date: 2023-12-21 22:36:29.386318

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6b729746663f'
down_revision = 'd8f0e6f9066a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('film', schema=None) as batch_op:
        batch_op.drop_column('depth')
        batch_op.drop_column('version')
        batch_op.drop_column('audio')

    with op.batch_alter_table('show', schema=None) as batch_op:
        batch_op.add_column(sa.Column('film_depth', sa.String(length=64), nullable=True))
        batch_op.add_column(sa.Column('film_audio', sa.String(length=64), nullable=True))
        batch_op.add_column(sa.Column('film_version', sa.String(length=64), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('show', schema=None) as batch_op:
        batch_op.drop_column('film_version')
        batch_op.drop_column('film_audio')
        batch_op.drop_column('film_depth')

    with op.batch_alter_table('film', schema=None) as batch_op:
        batch_op.add_column(sa.Column('audio', sa.VARCHAR(length=64), nullable=True))
        batch_op.add_column(sa.Column('version', sa.VARCHAR(length=64), nullable=True))
        batch_op.add_column(sa.Column('depth', sa.VARCHAR(length=64), nullable=True))

    # ### end Alembic commands ###
