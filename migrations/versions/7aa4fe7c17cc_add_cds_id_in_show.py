"""Add cds ID in show

Revision ID: 7aa4fe7c17cc
Revises: f7276c703a60
Create Date: 2023-12-21 15:04:33.404652

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7aa4fe7c17cc"
down_revision = "f7276c703a60"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("show", schema=None) as batch_op:
        batch_op.add_column(sa.Column("cds_id", sa.Integer(), nullable=False))
        batch_op.create_index(
            batch_op.f("ix_show_cds_id"), ["cds_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("show", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_show_cds_id"))
        batch_op.drop_column("cds_id")
