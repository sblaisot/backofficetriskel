"""Make Film attributes nullable

Revision ID: d8f0e6f9066a
Revises: 7aa4fe7c17cc
Create Date: 2023-12-21 15:29:06.199408

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d8f0e6f9066a"
down_revision = "7aa4fe7c17cc"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("film", schema=None) as batch_op:
        batch_op.alter_column(
            "storyline", existing_type=sa.TEXT(), nullable=True
        )
        batch_op.alter_column(
            "depth", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "audio", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "version", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "rating", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "visa", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "cds_id", existing_type=sa.INTEGER(), nullable=True
        )
        batch_op.alter_column(
            "allocine_id", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "trailerUrl", existing_type=sa.VARCHAR(length=256), nullable=True
        )
        batch_op.alter_column(
            "posterUrl", existing_type=sa.VARCHAR(length=256), nullable=True
        )
        batch_op.alter_column(
            "country", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "genre", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "director", existing_type=sa.VARCHAR(length=64), nullable=True
        )
        batch_op.alter_column(
            "cast", existing_type=sa.VARCHAR(length=256), nullable=True
        )


def downgrade():
    with op.batch_alter_table("film", schema=None) as batch_op:
        batch_op.alter_column(
            "cast", existing_type=sa.VARCHAR(length=256), nullable=False
        )
        batch_op.alter_column(
            "director", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "genre", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "country", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "posterUrl", existing_type=sa.VARCHAR(length=256), nullable=False
        )
        batch_op.alter_column(
            "trailerUrl", existing_type=sa.VARCHAR(length=256), nullable=False
        )
        batch_op.alter_column(
            "allocine_id", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "cds_id", existing_type=sa.INTEGER(), nullable=False
        )
        batch_op.alter_column(
            "visa", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "rating", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "version", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "audio", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "depth", existing_type=sa.VARCHAR(length=64), nullable=False
        )
        batch_op.alter_column(
            "storyline", existing_type=sa.TEXT(), nullable=False
        )
