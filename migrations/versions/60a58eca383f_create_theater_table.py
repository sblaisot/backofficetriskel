"""Create theater table

Revision ID: 60a58eca383f
Revises: 8582b0c24007
Create Date: 2023-12-19 12:07:40.526802

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "60a58eca383f"
down_revision = "8582b0c24007"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "theater",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=1024), nullable=False),
        sa.Column("city", sa.String(length=256), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_theater")),
    )
    with op.batch_alter_table("theater", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_theater_city"), ["city"], unique=False
        )

    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("theater_id", sa.Integer(), nullable=False)
        )
        batch_op.create_index(
            batch_op.f("ix_user_theater_id"), ["theater_id"], unique=False
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_user_theater_id_theater"),
            "theater",
            ["theater_id"],
            ["id"],
        )


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_user_theater_id_theater"), type_="foreignkey"
        )
        batch_op.drop_index(batch_op.f("ix_user_theater_id"))
        batch_op.drop_column("theater_id")

    with op.batch_alter_table("theater", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_theater_city"))

    op.drop_table("theater")
