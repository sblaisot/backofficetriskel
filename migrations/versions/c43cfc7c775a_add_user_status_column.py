"""Add User status column

Revision ID: c43cfc7c775a
Revises: 3224f2caa431
Create Date: 2023-12-25 23:17:15.341272

"""
from alembic import op
import sqlalchemy as sa

from app.models import User, UserStatus

# revision identifiers, used by Alembic.
revision = "c43cfc7c775a"
down_revision = "3224f2caa431"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "status",
                sa.Enum("PENDING", "ACTIVE", "DISABLED", name="userstatus"),
                nullable=True,
            )
        )

    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)

    op.execute(sa.update(User).values(status=UserStatus.ACTIVE))

    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.alter_column(
            "status", existing_type=sa.VARCHAR(length=8), nullable=False
        )


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_column("status")
