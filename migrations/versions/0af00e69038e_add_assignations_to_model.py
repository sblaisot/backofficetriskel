"""Add assignations to model

Revision ID: 0af00e69038e
Revises: ee9e82e23136
Create Date: 2023-12-29 21:54:05.058638

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0af00e69038e"
down_revision = "ee9e82e23136"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "assignation",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("theater_id", sa.Integer(), nullable=False),
        sa.Column("show_id", sa.Integer(), nullable=False),
        sa.Column("position_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["position_id"],
            ["position.id"],
            name=op.f("fk_assignation_position_id_position"),
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["show_id"],
            ["show.id"],
            name=op.f("fk_assignation_show_id_show"),
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["theater_id"],
            ["theater.id"],
            name=op.f("fk_assignation_theater_id_theater"),
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user.id"],
            name=op.f("fk_assignation_user_id_user"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_assignation")),
    )
    with op.batch_alter_table("assignation", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_assignation_position_id"),
            ["position_id"],
            unique=False,
        )
        batch_op.create_index(
            batch_op.f("ix_assignation_show_id"), ["show_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_assignation_theater_id"),
            ["theater_id"],
            unique=False,
        )
        batch_op.create_index(
            batch_op.f("ix_assignation_user_id"), ["user_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("assignation", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_assignation_user_id"))
        batch_op.drop_index(batch_op.f("ix_assignation_theater_id"))
        batch_op.drop_index(batch_op.f("ix_assignation_show_id"))
        batch_op.drop_index(batch_op.f("ix_assignation_position_id"))

    op.drop_table("assignation")
