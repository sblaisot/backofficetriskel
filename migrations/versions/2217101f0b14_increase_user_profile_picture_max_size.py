"""Increase user profile picture max size

Revision ID: 2217101f0b14
Revises: 86eba6af7a74
Create Date: 2024-11-17 01:05:35.773412

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '2217101f0b14'
down_revision = '86eba6af7a74'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('status',
               existing_type=mysql.VARCHAR(length=8),
               type_=sa.Enum('PENDING', 'ACTIVE', 'DISABLED', 'BANNED', name='userstatus'),
               existing_nullable=False)
        batch_op.alter_column('profile_picture',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=10000000),
               existing_nullable=True)

def downgrade():
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('profile_picture',
               existing_type=sa.Text(length=10000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
        batch_op.alter_column('status',
               existing_type=sa.Enum('PENDING', 'ACTIVE', 'DISABLED', 'BANNED', name='userstatus'),
               type_=mysql.VARCHAR(length=8),
               existing_nullable=False)
