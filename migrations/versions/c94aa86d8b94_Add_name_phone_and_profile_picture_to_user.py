"""Add name, phone and profile picture to user

Revision ID: c94aa86d8b94
Revises: 62dbe3993a69
Create Date: 2024-10-21 08:29:21.775711

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c94aa86d8b94'
down_revision = '62dbe3993a69'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('first_name', sa.String(length=64), nullable=True))
        batch_op.add_column(sa.Column('last_name', sa.String(length=64), nullable=True))
        batch_op.add_column(sa.Column('telephone', sa.String(length=20), nullable=True))
        batch_op.add_column(sa.Column('mobile_phone', sa.String(length=20), nullable=True))
        batch_op.add_column(sa.Column('profile_picture', sa.Text(10000000), nullable=True))


def downgrade():
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.drop_column('profile_picture')
        batch_op.drop_column('mobile_phone')
        batch_op.drop_column('telephone')
        batch_op.drop_column('last_name')
        batch_op.drop_column('first_name')
