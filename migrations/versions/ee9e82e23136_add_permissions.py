"""Add Permissions

Revision ID: ee9e82e23136
Revises: c43cfc7c775a
Create Date: 2023-12-26 18:44:35.294074

"""
from alembic import op
import sqlalchemy as sa

from app.models import Theater, Group, User

# revision identifiers, used by Alembic.
revision = "ee9e82e23136"
down_revision = "c43cfc7c775a"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "permission",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("theater_id", sa.Integer(), nullable=False),
        sa.Column("group_id", sa.Integer(), nullable=False),
        sa.Column("position_id", sa.Integer(), nullable=False),
        sa.Column(
            "permission",
            sa.Enum("NONE", "READ", "TAKE", "ASSIGN", name="permissiontypes"),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(
            ["group_id"],
            ["group.id"],
            name=op.f("fk_permission_group_id_group"),
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["position_id"],
            ["position.id"],
            name=op.f("fk_permission_position_id_position"),
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["theater_id"],
            ["theater.id"],
            name=op.f("fk_permission_theater_id_theater"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_permission")),
    )
    with op.batch_alter_table("permission", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_permission_group_id"), ["group_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_permission_position_id"),
            ["position_id"],
            unique=False,
        )
        batch_op.create_index(
            batch_op.f("ix_permission_theater_id"),
            ["theater_id"],
            unique=False,
        )

    with op.batch_alter_table("group", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("theater_admin", sa.Boolean(), nullable=False)
        )

    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.add_column(sa.Column("group_id", sa.Integer(), nullable=True))
        batch_op.create_index(
            batch_op.f("ix_user_group_id"), ["group_id"], unique=False
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_user_group_id_group"),
            "group",
            ["group_id"],
            ["id"],
            ondelete="SET NULL",
        )

    # Commented out because it causes troubles with updated Theater class with added cds_import_url attribute
    # bind = op.get_bind()
    # session = sa.orm.Session(bind=bind)
    # # We add a new admin group to all existing users
    # for theater in session.query(Theater):
    #     g = Group(name="Administrateur", theater=theater, theater_admin=True)
    #     session.add(g)
    #     for user in session.query(User).where(User.theater == theater):
    #         user.group = g
    # session.commit()


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_user_group_id_group"), type_="foreignkey"
        )
        batch_op.drop_index(batch_op.f("ix_user_group_id"))
        batch_op.drop_column("group_id")

    with op.batch_alter_table("group", schema=None) as batch_op:
        batch_op.drop_column("theater_admin")

    with op.batch_alter_table("permission", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_permission_theater_id"))
        batch_op.drop_index(batch_op.f("ix_permission_position_id"))
        batch_op.drop_index(batch_op.f("ix_permission_group_id"))

    op.drop_table("permission")
