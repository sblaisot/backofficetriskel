"""Create audtorium table

Revision ID: 13e40d2c1e4e
Revises: 60a58eca383f
Create Date: 2023-12-19 12:08:29.305750

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "13e40d2c1e4e"
down_revision = "60a58eca383f"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "auditorium",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=256), nullable=False),
        sa.Column("capacity", sa.Integer(), nullable=False),
        sa.Column("theater_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["theater_id"],
            ["theater.id"],
            name=op.f("fk_auditorium_theater_id_theater"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_auditorium")),
    )
    with op.batch_alter_table("auditorium", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_auditorium_theater_id"),
            ["theater_id"],
            unique=False,
        )


def downgrade():
    with op.batch_alter_table("auditorium", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_auditorium_theater_id"))

    op.drop_table("auditorium")
