"""Add group, film, show and position

Revision ID: 6ef0b1964df2
Revises: 13e40d2c1e4e
Create Date: 2023-12-21 11:31:50.652188

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6ef0b1964df2"
down_revision = "13e40d2c1e4e"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "film",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("title", sa.String(length=256), nullable=False),
        sa.Column("storyline", sa.Text(), nullable=False),
        sa.Column("depth", sa.String(length=64), nullable=False),
        sa.Column("audio", sa.String(length=64), nullable=False),
        sa.Column("version", sa.String(length=64), nullable=False),
        sa.Column("rating", sa.String(length=64), nullable=False),
        sa.Column("visa", sa.String(length=64), nullable=False),
        sa.Column("allocine_id", sa.String(length=64), nullable=False),
        sa.Column("trailerUrl", sa.String(length=256), nullable=False),
        sa.Column("posterUrl", sa.String(length=256), nullable=False),
        sa.Column("country", sa.String(length=64), nullable=False),
        sa.Column("genre", sa.String(length=64), nullable=False),
        sa.Column("director", sa.String(length=64), nullable=False),
        sa.Column("cast", sa.String(length=256), nullable=False),
        sa.Column("duration", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_film")),
    )
    with op.batch_alter_table("film", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_film_title"), ["title"], unique=False
        )

    op.create_table(
        "group",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=256), nullable=False),
        sa.Column("theater_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["theater_id"],
            ["theater.id"],
            name=op.f("fk_group_theater_id_theater"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_group")),
    )
    with op.batch_alter_table("group", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_group_theater_id"), ["theater_id"], unique=False
        )

    op.create_table(
        "position",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=256), nullable=False),
        sa.Column("theater_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["theater_id"],
            ["theater.id"],
            name=op.f("fk_position_theater_id_theater"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_position")),
    )
    with op.batch_alter_table("position", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_position_theater_id"), ["theater_id"], unique=False
        )

    op.create_table(
        "show",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("start", sa.DateTime(), nullable=False),
        sa.Column("end", sa.DateTime(), nullable=True),
        sa.Column("exclude_from_internet", sa.Boolean(), nullable=False),
        sa.Column("theater_id", sa.Integer(), nullable=False),
        sa.Column("auditorium_id", sa.Integer(), nullable=False),
        sa.Column("film_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["auditorium_id"],
            ["auditorium.id"],
            name=op.f("fk_show_auditorium_id_auditorium"),
        ),
        sa.ForeignKeyConstraint(
            ["film_id"], ["film.id"], name=op.f("fk_show_film_id_film")
        ),
        sa.ForeignKeyConstraint(
            ["theater_id"],
            ["theater.id"],
            name=op.f("fk_show_theater_id_theater"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_show")),
    )
    with op.batch_alter_table("show", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_show_auditorium_id"),
            ["auditorium_id"],
            unique=False,
        )
        batch_op.create_index(
            batch_op.f("ix_show_film_id"), ["film_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_show_theater_id"), ["theater_id"], unique=False
        )

    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("siteAdmin", sa.Boolean(), nullable=False)
        )


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_column("siteAdmin")

    with op.batch_alter_table("show", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_show_theater_id"))
        batch_op.drop_index(batch_op.f("ix_show_film_id"))
        batch_op.drop_index(batch_op.f("ix_show_auditorium_id"))

    op.drop_table("show")
    with op.batch_alter_table("position", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_position_theater_id"))

    op.drop_table("position")
    with op.batch_alter_table("group", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_group_theater_id"))

    op.drop_table("group")
    with op.batch_alter_table("film", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_film_title"))

    op.drop_table("film")
