"""Add MOTD

Revision ID: faef5eab54e4
Revises: 4cba91a94c44
Create Date: 2024-11-18 00:32:18.236294

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'faef5eab54e4'
down_revision = '4cba91a94c44'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('motd',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('message', sa.Text(), nullable=False),
        sa.Column('color', sa.String(length=32), nullable=False),
        sa.Column('start_date', sa.DateTime(), nullable=True),
        sa.Column('end_date', sa.DateTime(), nullable=True),
        sa.Column('theater_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['theater_id'], ['theater.id'], name=op.f('fk_motd_theater_id_theater'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_motd'))
    )
    with op.batch_alter_table('motd', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_motd_theater_id'), ['theater_id'], unique=False)

    op.create_table('motd_group',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('motd_id', sa.Integer(), nullable=False),
        sa.Column('group_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_motd_group_group_id_group'), ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['motd_id'], ['motd.id'], name=op.f('fk_motd_group_motd_id_motd'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_motd_group'))
    )
    with op.batch_alter_table('motd_group', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_motd_group_group_id'), ['group_id'], unique=False)
        batch_op.create_index(batch_op.f('ix_motd_group_motd_id'), ['motd_id'], unique=False)

    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('profile_picture',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=10000000),
               existing_nullable=True)


def downgrade():
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('profile_picture',
               existing_type=sa.Text(length=10000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)

    with op.batch_alter_table('motd_group', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_motd_group_motd_id'))
        batch_op.drop_index(batch_op.f('ix_motd_group_group_id'))

    op.drop_table('motd_group')
    with op.batch_alter_table('motd', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_motd_theater_id'))

    op.drop_table('motd')
