"""Add cds auditorium number

Revision ID: 8e832d9777fd
Revises: 6ef0b1964df2
Create Date: 2023-12-21 13:57:34.681048

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8e832d9777fd"
down_revision = "6ef0b1964df2"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("auditorium", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("cds_auditorium_number", sa.Integer(), nullable=True)
        )
        batch_op.create_index(
            batch_op.f("ix_auditorium_cds_auditorium_number"),
            ["cds_auditorium_number"],
            unique=False,
        )


def downgrade():
    with op.batch_alter_table("auditorium", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_auditorium_cds_auditorium_number"))
        batch_op.drop_column("cds_auditorium_number")
