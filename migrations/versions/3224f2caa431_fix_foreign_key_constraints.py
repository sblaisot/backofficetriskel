"""Fix foreign key constraints

Revision ID: 3224f2caa431
Revises: 6b729746663f
Create Date: 2023-12-24 01:37:18.003367

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "3224f2caa431"
down_revision = "6b729746663f"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("auditorium", schema=None) as batch_op:
        batch_op.drop_constraint(
            "fk_auditorium_theater_id_theater", type_="foreignkey"
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_auditorium_theater_id_theater"),
            "theater",
            ["theater_id"],
            ["id"],
            ondelete="CASCADE",
        )

    with op.batch_alter_table("group", schema=None) as batch_op:
        batch_op.drop_constraint(
            "fk_group_theater_id_theater", type_="foreignkey"
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_group_theater_id_theater"),
            "theater",
            ["theater_id"],
            ["id"],
            ondelete="CASCADE",
        )

    with op.batch_alter_table("position", schema=None) as batch_op:
        batch_op.drop_constraint(
            "fk_position_theater_id_theater", type_="foreignkey"
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_position_theater_id_theater"),
            "theater",
            ["theater_id"],
            ["id"],
            ondelete="CASCADE",
        )

    with op.batch_alter_table("show", schema=None) as batch_op:
        batch_op.drop_constraint("fk_show_film_id_film", type_="foreignkey")
        batch_op.drop_constraint(
            "fk_show_theater_id_theater", type_="foreignkey"
        )
        batch_op.drop_constraint(
            "fk_show_auditorium_id_auditorium", type_="foreignkey"
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_show_film_id_film"),
            "film",
            ["film_id"],
            ["id"],
            ondelete="CASCADE",
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_show_theater_id_theater"),
            "theater",
            ["theater_id"],
            ["id"],
            ondelete="CASCADE",
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_show_auditorium_id_auditorium"),
            "auditorium",
            ["auditorium_id"],
            ["id"],
            ondelete="CASCADE",
        )

    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_constraint(
            "fk_user_theater_id_theater", type_="foreignkey"
        )
        batch_op.create_foreign_key(
            batch_op.f("fk_user_theater_id_theater"),
            "theater",
            ["theater_id"],
            ["id"],
            ondelete="CASCADE",
        )


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_user_theater_id_theater"), type_="foreignkey"
        )
        batch_op.create_foreign_key(
            "fk_user_theater_id_theater", "theater", ["theater_id"], ["id"]
        )

    with op.batch_alter_table("show", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_show_auditorium_id_auditorium"), type_="foreignkey"
        )
        batch_op.drop_constraint(
            batch_op.f("fk_show_theater_id_theater"), type_="foreignkey"
        )
        batch_op.drop_constraint(
            batch_op.f("fk_show_film_id_film"), type_="foreignkey"
        )
        batch_op.create_foreign_key(
            "fk_show_auditorium_id_auditorium",
            "auditorium",
            ["auditorium_id"],
            ["id"],
        )
        batch_op.create_foreign_key(
            "fk_show_theater_id_theater", "theater", ["theater_id"], ["id"]
        )
        batch_op.create_foreign_key(
            "fk_show_film_id_film", "film", ["film_id"], ["id"]
        )

    with op.batch_alter_table("position", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_position_theater_id_theater"), type_="foreignkey"
        )
        batch_op.create_foreign_key(
            "fk_position_theater_id_theater", "theater", ["theater_id"], ["id"]
        )

    with op.batch_alter_table("group", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_group_theater_id_theater"), type_="foreignkey"
        )
        batch_op.create_foreign_key(
            "fk_group_theater_id_theater", "theater", ["theater_id"], ["id"]
        )

    with op.batch_alter_table("auditorium", schema=None) as batch_op:
        batch_op.drop_constraint(
            batch_op.f("fk_auditorium_theater_id_theater"), type_="foreignkey"
        )
        batch_op.create_foreign_key(
            "fk_auditorium_theater_id_theater",
            "theater",
            ["theater_id"],
            ["id"],
        )
