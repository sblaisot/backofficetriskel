"""Add cds_import_url to theater

Revision ID: 86eba6af7a74
Revises: 7c1c9d2daa42
Create Date: 2024-11-04 12:03:16.390628

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '86eba6af7a74'
down_revision = '7c1c9d2daa42'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('theater', schema=None) as batch_op:
        batch_op.add_column(sa.Column('cds_import_url', sa.String(length=1024), nullable=True))

def downgrade():
    with op.batch_alter_table('theater', schema=None) as batch_op:
        batch_op.drop_column('cds_import_url')
