"""show.cds_id can be null

Revision ID: 7c1c9d2daa42
Revises: c94aa86d8b94
Create Date: 2024-11-04 00:28:56.311438

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7c1c9d2daa42'
down_revision = 'c94aa86d8b94'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('show', schema=None) as batch_op:
        batch_op.alter_column('cds_id',
               existing_type=sa.INTEGER(),
               nullable=True)


def downgrade():
    with op.batch_alter_table('show', schema=None) as batch_op:
        batch_op.alter_column('cds_id',
               existing_type=sa.INTEGER(),
               nullable=False)
