"""Add assignable model

Revision ID: 62dbe3993a69
Revises: 0af00e69038e
Create Date: 2024-02-27 15:40:15.485864

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "62dbe3993a69"
down_revision = "0af00e69038e"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "assignable",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("position_id", sa.Integer(), nullable=False),
        sa.Column("group_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["group_id"],
            ["group.id"],
            name=op.f("fk_assignable_group_id_group"),
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["position_id"],
            ["position.id"],
            name=op.f("fk_assignable_position_id_position"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_assignable")),
    )
    with op.batch_alter_table("assignable", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_assignable_group_id"), ["group_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_assignable_position_id"),
            ["position_id"],
            unique=False,
        )


def downgrade():
    with op.batch_alter_table("assignable", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_assignable_position_id"))
        batch_op.drop_index(batch_op.f("ix_assignable_group_id"))

    op.drop_table("assignable")
