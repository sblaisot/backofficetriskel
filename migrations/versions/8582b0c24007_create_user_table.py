"""Create user table

Revision ID: 8582b0c24007
Revises:
Create Date: 2023-12-19 12:06:54.105533

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8582b0c24007"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "user",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("username", sa.String(length=64), nullable=False),
        sa.Column("email", sa.String(length=120), nullable=False),
        sa.Column("password_hash", sa.String(length=256), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_user")),
    )
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_user_email"), ["email"], unique=True
        )
        batch_op.create_index(
            batch_op.f("ix_user_username"), ["username"], unique=True
        )


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_user_username"))
        batch_op.drop_index(batch_op.f("ix_user_email"))

    op.drop_table("user")
