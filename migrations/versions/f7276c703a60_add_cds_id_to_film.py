"""Add cds_id to film

Revision ID: f7276c703a60
Revises: 8e832d9777fd
Create Date: 2023-12-21 14:48:03.070979

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "f7276c703a60"
down_revision = "8e832d9777fd"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("film", schema=None) as batch_op:
        batch_op.add_column(sa.Column("cds_id", sa.Integer(), nullable=False))
        batch_op.create_index(
            batch_op.f("ix_film_cds_id"), ["cds_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("film", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_film_cds_id"))
        batch_op.drop_column("cds_id")
