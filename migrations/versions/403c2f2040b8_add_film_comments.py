"""Add film comments

Revision ID: 403c2f2040b8
Revises: 3bda9dbe8eb7
Create Date: 2024-11-28 23:16:07.931582

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '403c2f2040b8'
down_revision = '3bda9dbe8eb7'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('film_comment',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('message', sa.Text(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('author_id', sa.Integer(), nullable=True),
        sa.Column('film_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['author_id'], ['user.id'], name=op.f('fk_film_comment_author_id_user'), ondelete='SET NULL'),
        sa.ForeignKeyConstraint(['film_id'], ['film.id'], name=op.f('fk_film_comment_film_id_film'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_film_comment'))
    )
    with op.batch_alter_table('film_comment', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_film_comment_film_id'), ['film_id'], unique=False)

    op.create_table('film_comment_target',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('comment_id', sa.Integer(), nullable=False),
        sa.Column('group_id', sa.Integer(), nullable=True),
        sa.Column('position_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['comment_id'], ['film_comment.id'], name=op.f('fk_film_comment_target_comment_id_film_comment'), ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_film_comment_target_group_id_group'), ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['position_id'], ['position.id'], name=op.f('fk_film_comment_target_position_id_position'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_film_comment_target'))
    )
    with op.batch_alter_table('film_comment_target', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_film_comment_target_comment_id'), ['comment_id'], unique=False)
        batch_op.create_index(batch_op.f('ix_film_comment_target_group_id'), ['group_id'], unique=False)
        batch_op.create_index(batch_op.f('ix_film_comment_target_position_id'), ['position_id'], unique=False)

def downgrade():
    with op.batch_alter_table('film_comment_target', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('fk_film_comment_target_position_id_position'), type_='foreignkey')
        batch_op.drop_constraint(batch_op.f('fk_film_comment_target_group_id_group'), type_='foreignkey')
        batch_op.drop_constraint(batch_op.f('fk_film_comment_target_comment_id_film_comment'), type_='foreignkey')
        batch_op.drop_index(batch_op.f('ix_film_comment_target_position_id'))
        batch_op.drop_index(batch_op.f('ix_film_comment_target_group_id'))
        batch_op.drop_index(batch_op.f('ix_film_comment_target_comment_id'))
    op.drop_table('film_comment_target')

    with op.batch_alter_table('film_comment', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('fk_film_comment_film_id_film'), type_='foreignkey')
        batch_op.drop_constraint(batch_op.f('fk_film_comment_author_id_user'), type_='foreignkey')
        batch_op.drop_index(batch_op.f('ix_film_comment_film_id'))
    op.drop_table('film_comment')
