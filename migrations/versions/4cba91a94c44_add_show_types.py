"""Add show types

Revision ID: 4cba91a94c44
Revises: 2217101f0b14
Create Date: 2024-11-17 01:17:35.494782

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '4cba91a94c44'
down_revision = '2217101f0b14'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('showtype',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(length=64), nullable=False),
        sa.Column('color', sa.String(length=64), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('theater_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['theater_id'], ['theater.id'], name=op.f('fk_showtype_theater_id_theater'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_showtype'))
    )
    with op.batch_alter_table('showtype', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_showtype_theater_id'), ['theater_id'], unique=False)

    with op.batch_alter_table('show', schema=None) as batch_op:
        batch_op.add_column(sa.Column('showtype_id', sa.Integer(), nullable=True))
        batch_op.create_index(batch_op.f('ix_show_showtype_id'), ['showtype_id'], unique=False)
        batch_op.create_foreign_key(batch_op.f('fk_show_showtype_id_showtype'), 'showtype', ['showtype_id'], ['id'], ondelete='SET NULL')

    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('profile_picture',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=10000000),
               existing_nullable=True)


def downgrade():
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.alter_column('profile_picture',
               existing_type=sa.Text(length=10000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)

    with op.batch_alter_table('show', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('fk_show_showtype_id_showtype'), type_='foreignkey')
        batch_op.drop_index(batch_op.f('ix_show_showtype_id'))
        batch_op.drop_column('showtype_id')

    with op.batch_alter_table('showtype', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_showtype_theater_id'))

    op.drop_table('showtype')
