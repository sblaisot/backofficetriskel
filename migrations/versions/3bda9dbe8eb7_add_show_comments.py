"""Add show comments

Revision ID: 3bda9dbe8eb7
Revises: 79ab8bc81fda
Create Date: 2024-11-20 22:20:59.387578

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '3bda9dbe8eb7'
down_revision = 'faef5eab54e4'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('show_comment',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('message', sa.Text(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('author_id', sa.Integer(), nullable=True),
        sa.Column('show_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['author_id'], ['user.id'], name=op.f('fk_show_comment_author_id_user'), ondelete='SET NULL'),
        sa.ForeignKeyConstraint(['show_id'], ['show.id'], name=op.f('fk_show_comment_show_id_show'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_show_comment'))
    )
    with op.batch_alter_table('show_comment', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_show_comment_show_id'), ['show_id'], unique=False)

    op.create_table('show_comment_target',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('comment_id', sa.Integer(), nullable=False),
        sa.Column('group_id', sa.Integer(), nullable=True),
        sa.Column('position_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['comment_id'], ['show_comment.id'], name=op.f('fk_show_comment_target_comment_id_show_comment'), ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_show_comment_target_group_id_group'), ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['position_id'], ['position.id'], name=op.f('fk_show_comment_target_position_id_position'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_show_comment_target'))
    )
    with op.batch_alter_table('show_comment_target', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_show_comment_target_comment_id'), ['comment_id'], unique=False)
        batch_op.create_index(batch_op.f('ix_show_comment_target_group_id'), ['group_id'], unique=False)
        batch_op.create_index(batch_op.f('ix_show_comment_target_position_id'), ['position_id'], unique=False)


def downgrade():
    with op.batch_alter_table('show_comment_target', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_show_comment_target_position_id'))
        batch_op.drop_index(batch_op.f('ix_show_comment_target_group_id'))
        batch_op.drop_index(batch_op.f('ix_show_comment_target_comment_id'))

    op.drop_table('show_comment_target')
    with op.batch_alter_table('show_comment', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_show_comment_show_id'))

    op.drop_table('show_comment')
