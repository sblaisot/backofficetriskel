SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ifeq ($(origin .RECIPEPREFIX), undefined)
	$(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

prune-branches:
> git fetch -p && LANG=C git branch -vv | awk '/: gone]/&&!/^*/{print $$1}' | xargs -r -- git branch -d
.PNONY: prune-branches

PIP_COMPILE := pip-compile -q --no-header --allow-unsafe --resolver=backtracking

constraints.txt: *.in
> CONSTRAINTS=/dev/null $(PIP_COMPILE) --strip-extras -o $@ $^

requirements.txt: requirements.in constraints.txt
> CONSTRAINTS=constraints.txt $(PIP_COMPILE) -o $@ $<

requirements-dev.txt: requirements-dev.in requirements.in constraints.txt
> CONSTRAINTS=constraints.txt $(PIP_COMPILE) -o $@ requirements-dev.in requirements.in

requirements: constraints.txt $(addsuffix .txt, $(basename $(wildcard *.in)))
.PHONY: requirements

clean:
> rm -rf constraints.txt $(addsuffix .txt, $(basename $(wildcard *.in)))

update-requirements: clean requirements

venv: requirements.txt
> python3 -m venv venv
> venv/bin/pip install wheel
> venv/bin/pip install -r $^

venv-dev: requirements-dev.txt
> python3 -m venv venv-dev
> venv-dev/bin/pip install wheel
> venv-dev/bin/pip install -r $^

# lint: venv-dev
# > venv-dev/bin/black --check --diff --color .
# .PHONY: lint

# flake8: venv-dev
# > venv-dev/bin/flake8 bender
# .PHONY: flake8

# node_modules:
# > yarn install

# prettier: node_modules
# > node_modules/.bin/prettier --check **/*.js
# .PHONY: prettier

# test: venv-dev
# > venv-dev/bin/pytest --junitxml=test_report.xml --html=test_report.html --self-contained-html --color=yes
# .PHONY: test

# serve: venv-dev
# > venv-dev/bin/flask run
# .PHONY: serve
