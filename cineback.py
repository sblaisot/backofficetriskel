import sqlalchemy as sa
import sqlalchemy.orm as so

from werkzeug.middleware.proxy_fix import ProxyFix

from app import create_app, db
from app.cli import register_commands
from app.models import (
    User,
    UserStatus,
    Theater,
    Auditorium,
    Group,
    Film,
    Position,
    PermissionTypes,
    Permission,
    Assignation,
    load_user,
)

app = create_app()
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1)
register_commands(app)

@app.shell_context_processor
def make_shell_context():
    return {
        "sa": sa,
        "so": so,
        "db": db,
        "User": User,
        "UserStatus": UserStatus,
        "Theater": Theater,
        "Auditorium": Auditorium,
        "Group": Group,
        "Film": Film,
        "Position": Position,
        "Permission": Permission,
        "PermissionTypes": PermissionTypes,
        "Assignation": Assignation,
        "load_user": load_user,
    }
